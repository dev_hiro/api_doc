---
title: Daikin IoT (PoC) Cloud API V1

language_tabs:
  - https

toc_footers:


includes:


search: true
---

# Introduction

<span style="font-size: 26px;font-weight:bold">Daikin IoT (PoC) Cloud API V1</span>

This document is intended for developers who want to use the data, intelligence, and services provided by one or more of the Daikin IoT APIs in their applications.

A proper Daikin IoT API request must be of the following form:

`https://www.daikinlab.com/api/?parameters`

To access the Daikin API over HTTP, use:

`http://www.daikinlab.com/api/?parameters`

While making an API request some parameters are required while some are optional. As is standard in URLs, parameters are separated using the ampersand (&) character.

You need to have a valid API Key to use this API. This is either the JWT (JASON Web Token) received on authentication (see [User Authentication](./#Authentication) below) or the fixed (non expiring) API key assigned to a Developer user. If you are registered as a developer, this key will be displayed on your user profile page as API Key. All other users need to authenticate and use the JWT as the API key. The API key can be passed in the request header (recommended) with key apikey or as part of the URL.

## Testing
Please note that `https://www.daikinlab.com/api` is the site serving applications with live devices and data. If you are in the process of using these API for development and testing, please use `http://dev.daikinlab.com/api` instead. This development site has the exact same API support but with non production data. While it mirrors all the features of the production site it may not offer the same performance or scalability of the production server. So use it to test the API and features but for testing the scalability or performance of these APIs.

# User Authentication

## Authenticate User

> Examples:

> https://daikinlab.com/api/user/authenticate?email=tom@awesome.com&password=awesome <br/>
> The above authenticates user with email id tom@awesome.com with password awesome

`POST /api/user/authenticate?userId=EMAIL_ID&password=PASSWORD`

<b>This authenticates a user. </b>
The `EMAIL_ID` and `PASSWORD` need to be the email id and password of a valid Daikin Iot user. 
It is recommended that both of these are set in the request header with keys userid and password instead of passing them as URL request parameters

### Required Parameters

Parameter | Value
--------- | -----------
| <b>One or more of the following:</b>
userId | Email ID of the user which is used as the userId.
email | Email ID of the user.
| <b>Always required</b>
password | Password of the Daikin IoT user

### Optional Parameters

None

### Responses:
The JWT is returned if the login is successful. 
The returned JWT can be used as the `apiKey` for subsequent API interactions with Daikin cloud. 
One of the following Error Message is returned if the operation failed:

Error | Description
--------- | -----------
NoUserIdOrEmail | Neither `userId` nor `email` specified.
NoPassword | `password` missing
InvalidLogin | If invalid `userId` or `password` specified


## Change Password

> Examples:

> https://daikinlab.com/api/user/changepassword?apiKey=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE1MzIwNzkzOTEsImtXttWtiOiI1OThiNzllNmRlNDczZmM4MjY0NTY1MDQiLCJpYXQiOjE1MzIwMzYxOTF9.hTH8no6I3iP2QuQd89oE8ulWg9GCkCVU0uDjH4-RsMk&password=notsoawesome 

> The above changes the password of the user with specified API key to notsoawesome.

`POST /api/user/changepassword?apiKey=JWT&password=NEW_PASSWORD`

<b>This lets the user to change the password.</b>

### Required Parameters

Parameter | Value
--------- | -----------
apiKey | JWT received on authentication.
password | The new password


### Optional Parameters

None

### Responses:

The number `1` is returned if the operation is successful. 

One of the following Error Message is returned if the operation failed:

Error | Description
--------- | -----------
NoApiKey | `apiKey` missing from the request.
NoPassword | `password` missing from the request.
ExpiredApiKey | If JWT specified is expired
InvalidApiKey | If JWT specified is invalid


## Reset Password

> Examples:

> https://daikinlab.com/api/user/resetpassword?userId=tom@awesome.com 


> The above will result resetting the password of user tom@awesome.com to a temporary one. The temporary password will be sent to tom@awesome.com.

`POST /api/user/resetpassword?userId=EMAIL_ID`

<b>This lets the user to request a password reset. </b>
Once the password request is received, the system shall send the user a temporary password by email. After logging in with the temporary password, the user needs to change the password to continue using the services.

### Required Parameters

Parameter | Value
--------- | -----------
| <b>One or more of the following:</b>
userId | Email ID of the user which is used as the userId.
email | Email ID of the user.

### Optional Parameters

None

### Responses:

The number `1` is returned if the operation is successful. 

One of the following Error Message is returned if the operation failed:

Error | Description
--------- | -----------
NoUserIdOrEmail | Neither `userId` nor `email` specified.
InvalidUserIdOrEmail | Specified `userId` nor `email` is invalid or not found.


## Check Change Password Needed

> Examples

> https://daikinlab.com/api/user/usingtemppassword

`GET /api/user/usingtemppassword`

<b>This checks whether the user needs to change the password. </b>
After resetting password and authenticating with the system genered password the user needs to change password. 
This API allows the caller to check whether the user is currently authenticated with the system generated temporary password or not.

### Responses

The word `true` is returned if password change is required. 
The word `false` is returned otherwords. 

One of the following Error Message is returned if the operation failed:

Error | Description
--------- | -----------
NoApiKey | `apiKey` missing from the request.
ExpiredApiKey | If JWT specified is expired.
InvalidApiKey | If JWT specified is invalid.


## Get User Detail

> Examples

> https://daikinlab.com/api/user/info?fields=name

`GET /api/user/info`

<b>This returns information about the currently logged in user. </b>
The available information includes `name`, `email`, and `role` .

### Required Parameters

Parameter | Value
--------- | -----------
| <b>One or more of the following:</b>
apiKey | JWT received on authentication.

### Optional parameters
Parameter | Value
--------- | -----------
fields | an array of fields, for ex. `fields[]=name&fields[]=email`. If none specified, all field values are returned

### Responses

> A sample output of a successful operation can be as follows:

```https
{
  "name": "Brad Wonders",
  "email": "brad@wonders.com",
  "role":"Daikin QA"
}
```

The user detail is returned in JSON format if the operation is successful. 

One of the following Error Message is returned if the operation failed:

Error | Description
--------- | -----------
NoApiKey | `apiKey` missing from the request.
ExpiredApiKey | If JWT specified is expired
InvalidApiKey | If JWT specified is invalid


# Device Data Requests

## Get Device Data Model 

<b>Describes the data model for a device type.</b>

For instance, the following returns the data model for the device type DEVICE_TYPE: 
`GET /api/data/model?deviceType=DEVICE_TYPE&apiKey=YOUR_API_KEY `

### Required Parameters

Parameter | Value
--------- | -----------
deviceType | The name of the device type to describe
apiKey | Your Daikin API key for authentication and access control

### Responses

> A sample output of a successful operation can be as follows: <br/>
> http://www.daikinlab.com/api/data/count?deviceType=OutdoorAirSensor&apiKey=YOUR_API_KEY

```https
{
    "aqi": {
        "type": "integer",
        "required": true
    },
    "color": {
        "type": "string",
        "required": true
    },
    "co": {
        "type": "float",
        "required": true
    },
    "no2": {
        "type": "float",
        "required": true
    },
    "o3": {
        "type": "float",
        "required": true
    },
    "pm25": {
        "type": "integer",
        "required": true
    },
    "pm10": {
        "type": "integer",
        "required": true
    },
    "so2": {
        "type": "float",
        "required": true
    },
    "device": {
        "model": "device",
        "required": true
    },
    "id": {
        "type": "string",
        "autoIncrement": true,
        "primaryKey": true,
        "unique": true
    },
    "createdAt": {
        "type": "datetime",
        "default": "NOW"
    }
}
```

The data model with its attributes and types are returned in JSON format. 


One of the following Error Message is returned if the operation failed:

Error | Description
--------- | -----------
NoDeviceType | Required `deviceType` missing from the request
InvalidDeviceType | The `deviceType` value specified is invalid
NoApiKey | `apiKey` missing from the request
ExpiredApiKey | If JWT specified is expired
InvalidApiKey | If JWT specified is invalid
AccessDenied | If the user does not have access to the device type specified


## Search Device Data 

> Examples

> The following request returns the second page of records, 100 rows at a time, from data collected from device 70:ee:50:27:97:cc after July 10, 2018 devices: <br/>
> http://www.daikinlab.com/api/data?deviceType=Netatmo&query={"device":"70:ee:50:27:97:cc","createdAt":{">":"2018-07-10"}}&apiKey=YOUR_API_KEY&rows=100&page=2 

> The following request returns just noise and temperature values alone from the first page of records, 10 rows at a time, from the data collected from device 70:ee:50:27:97:cc after July 10, 2018 devices: <br/>
> http://www.daikinlab.com/api/data?deviceType=Netatmo&query={"device":"70:ee:50:27:97:cc","createdAt":{">":"2018-07-10"}}&apiKey=YOUR_API_KEY&rows=100&page=2&fields=noise&fields[]=temperature

<b>Query the device data across all devices of specified Device Type.</b>
You can request all or portion of the data collected by your devices. 
All you need to know is the device type name and the query to filter the data. 
For instance, the following returns all the data collected from BigAC devices where the temp field values are greater than 30 degree celcius: 
`GET /api/data?deviceType=BigAC&query={"temp":{">":"30"}}&apiKey=YOUR_API_KEY `

### Required Parameters

Parameter | Value
--------- | -----------
deviceType | The name of the device type to search
query | the filter / query in JSON format for ex, if you are only interested in data from 3 devices with ids 30000c2a690cc54a, 30000c2a690ef696, and 30000c2a690ef699 with co2 reported greater than 1000 you would pass `{"device":["30000c2a690cc54a","30000c2a690ef696","30000c2a690ef699"],"co2":{">":"1000"}}`. All attributes in your device data model as described in Get Device Data Model are available to be used in the query.
apiKey | Your Daikin API key for authentication and access control.

### Optional parameters
Parameter | Value
--------- | -----------
sort | The sort condition for any field. For example, `temp ASC` if you want the results to be sorted in ascending order of the temp value. By default the results are sorted in descending order of the `createdAt` timestamp
page | the relative page of data to return, for ex. `2` to return 2nd page
rows | The number of records per page, for ex., `100`.
view | the name of the custom data view to apply if any.
transformation | the name of the data trasformation to apply if any.
fields | an array of fields, for ex. `fields[]=temp&fields[]=pressure`. If none specified, all field values are returned

### Responses

> In the example below, the data for BigAC devices are returned: <br/>
> http://www.daikinlab.com/api/data?deviceType=BigAC&apiKey=YOUR_API_KEY

```https
[
  {
    "device": "testdevice6",
    "temp": 20,
    "pressure": 1000,
    "humidity": 40,
    "createdAt": "2018-05-17T23:07:36.525Z",
    "id": "5afe0b3887fb58bc23501abe"
  },
  {
    "device": "testdevice6",
    "temp": 20,
    "pressure": 1000,
    "humidity": 40,
    "createdAt": "2018-05-17T23:07:23.960Z",
    "id": "5afe0b2b87fb58bc23501abd"
  },
  {
    "device": "testdevice6",
    "temp": 20,
    "pressure": 1000,
    "humidity": 40,
    "createdAt": "2018-05-17T23:03:25.922Z",
    "id": "5afe0a3d87fb58bc23501abc"
  },
  {
    "device": "23519ef04a48b8ee",
    "temp": 26.63,
    "pressure": 1011.46,
    "humidity": 42.46,
    "createdAt": "2017-09-26T18:29:14.414Z",
    "id": "59ca9c7a8aa155100076194b"
  },
  {
    "device": "23519ef04a48b8ee",
    "temp": 26.59,
    "pressure": 1011.7,
    "humidity": 43.1,
    "createdAt": "2017-09-26T17:59:12.883Z",
    "id": "59ca9570e1ae771000d9c490"
  },
  {
    "device": "23519ef04a48b8ee",
    "pressure": 1012.06,
    "humidity": 43.19,
    "createdAt": "2017-09-26T17:29:11.394Z",
    "id": "59ca8e678aa1551000761947"
  }
]
```

The responses are returned in JSON format. 

One of the following Error Message is returned if the operation failed:

Error | Description
--------- | -----------
NoDeviceType | Required `deviceType` missing from the request.
InvalidDeviceType | The `deviceType` value specified is invalid.
InvalidQuery | The query specified is invalid.
NoApiKey | `apiKey` missing from the request.
ExpiredApiKey | If JWT specified is expired
InvalidApiKey | If JWT specified is invalid
AccessDenied | If the user does not have access to the device type specified


## Count Search Results 

> Example: <br/>
> The following returns the number of data collected from BigAC devices where the temp field values are greater than 30 degree celcius: <br/>
> GET /api/data/count?deviceType=BigAC&query={"temp":{">":"30"}}&apiKey=YOUR_API_KEY 

<b>Count the number of rows that matches the device data search query. </b>
You can count all or portion of the data collected by your devices. 
All you need to know is the device type name and the query to filter the data. 

### Required Parameters

Parameter | Value
--------- | -----------
deviceType | The name of the device type to search
query | the filter / query in JSON format for ex, if you are only interested in data from 3 devices with ids 30000c2a690cc54a,30000c2a690ef696, and 30000c2a690ef699 with co2 reported greater than 1000 you would pass `{"device":["30000c2a690cc54a","30000c2a690ef696","30000c2a690ef699"],"co2":{">":"1000"}}`. All attributes in your device data model as described in Get Device Data Model are available to be used in the query.
apiKey | Your Daikin API key for authentication and access control.

### Responses

The count is returned as number if the request is successful. 

One of the following Error Message is returned if the operation failed:

Error | Description
--------- | -----------
NoDeviceType | Required `deviceType` missing from the request.
InvalidDeviceType | The `deviceType` value specified is invalid.
InvalidQuery | The query specified is invalid.
NoApiKey | `apiKey` missing from the request.
ExpiredApiKey | If JWT specified is expired
InvalidApiKey | If JWT specified is invalid
AccessDenied | If the user does not have access to the device type specified


## Get Device Data Thresholds 

`GET /api/data/thresholds?deviceType=DEVICE_TYPE&thresholdName=THRESHOLD_NAME&apiKey=YOUR_API_KEY `

<b>Returns the specific device data thresholds for the device type.</b>
Daikin IoT Platform allows applications to leverage configured device data thresholds to classify / color code device readings. 
There could be zero or more threshold settings per device type: 

### Required Parameters

Parameter | Value
--------- | -----------
deviceType | The name of the device type for which the threshold is created
thresholdName | The unique name of the threshold setting
apiKey | Your Daikin API key for authentication and access control.

### Responses

> A sample of a successful operation:

```https
{
  "temp":{
    "LOW_CRITICAL":{"val":62},
    "LOW_WARNING":{"val":63},
    "GOOD":{"val":65},
    "WARNING":{"val":187},
    "CRITICAL":{"val":189}
  },
  "pressure":{
    "LOW_CRITICAL":{"val":34},
    "LOW_WARNING":{"val":35},
    "GOOD":{"val":40},
    "WARNING":{"val":55},
    "CRITICAL":{"val":60}
  },
  "pm25":{
    "LOW_CRITICAL":{"val":-0.2},
    "LOW_WARNING":{"val":-0.1},
    "GOOD":{"val":0},
    "WARNING":{"val":800},
    "CRITICAL":{"val":1000}},
  "co2":{
    "LOW_CRITICAL":{"val":-0.11},
    "LOW_WARNING":{"val":-0.1},
    "GOOD":{"val":0},
    "WARNING":{"val":801},
    "CRITICAL":{"val":1001}
  },
  "tvoc":{
    "LOW_CRITICAL":{"val":-0.12},
    "LOW_WARNING":{"val":-0.1},
    "GOOD":{"val":0},
    "WARNING":{"val":500},
    "CRITICAL":{"val":3000}
  }
}
```

The threshold setting is returned as JSON if the request is successful.

One of the following Error Message is returned if the operation failed:

Error | Description
--------- | -----------
NoDeviceType | Required `deviceType` missing from the request.
InvalidDeviceType | The `deviceType` value specified is invalid.
NoThresholdName | Required `thresholdName` missing from the request.
InvalidThresholdName | The `thresholdName` value specified is invalid.
NoApiKey | `apiKey` missing from the request.
ExpiredApiKey | If JWT specified is expired
InvalidApiKey | If JWT specified is invalid
AccessDenied | If the user does not have access to the device type specified


# Analytics Data Requests
Daikin IoT platfom computes at regular intervals the analytics including the following for all numeric values reported by the device.
- Mean
- Median
- Mode
- Minimum
- Maximum

The analytics is calculated for every device for all its numeric attributes aggregated for the following periods:
- Hourly
- Daily
- Weekly
- Monthly
- Yearly

For instance, if a device reports temperature, these APIs can then be used to find the computed hourly, daily, weekly, monthly, and yearly temperature trends as reported by that device.

## Get Analytics Attributes 

`GET api/analytics/attributes?apiKey=API_KEY&deviceId=DEVICE_ID `

<b>List the attributes for which the analytics have been computed for the specified device.</b>
Only numeric attributes reported by the device are considered for the analytics computations. 


### Required parameters

Parameter | Value
--------- | -----------
deviceId | The device identifier
apiKey | Your Daikin API key for authentication and access control.


### Responses
> A sample of a successful operation <br/>
> http://www.daikinlab.com/api/analytics/attributes?apiKey=API_KEY&deviceId=DEVICE_ID

```https
["ch20","co","co2","dB_average","dB_max","dB_min","humidity","lux","no2","o3","pm1","pm10","pm25","pressure","so2","temp","tvoc"]
```

The list of attributes are returned in JSON format. 

One of the following Error Message is returned if the operation failed:

Error | Description
--------- | -----------
NoDeviceId | Required `deviceId` missing from the request.
InvalidDeviceId | The `deviceId` value specified is invalid.
NoApiKey | `apiKey` missing from the request.
ExpiredApiKey | If JWT specified is expired
InvalidApiKey | If JWT specified is invalid


## Search Analytics Data 

> Examples:
The following returns the mean Weekly temperature trends from device DEVICE_ID: <br/>
> GET api/analytics/data?deviceId=DEVICE_ID&attribute=temp&period=Weekly&fields=mean&&fields=periodEnd&apiKey=YOUR_API_KEY 


>The following request returns the second page of maximum CO2 readings, 100 rows at a time, from data collected from the device 70:ee:50:27:97:cc after August 20, 2018: <br/>
> http://www.daikinlab.com/api/analytics/data?deviceId=70:ee:50:27:97:cc&attribute=temp&period=Weekly&fields=mean&fields=periodEnd&query={"periodEnd":{">":"2018-08-20"}}&apiKey=YOUR_API_KEY&rows=100&page=2

<b>Query the analytics data for the specified attribute of a device </b>
You can request all or portion of the analytics data for a device. 

All you need to know is the period type (Hourly, Weekly, etc) and the query to filter the data. 

### Required parameters

Parameter | Value
--------- | -----------
deviceId | The id of the device to search analytics for
period | One of the following (case sensitive): <br/> Hourly<br/> Daily<br/> Weekly<br/> Monthly<br/> Yearly
attribute | One of the attributes with numeric values. See [Get Analytics Attributes](./#Get-Analytics-Attributes) for more info.
apiKey | Your Daikin API key for authentication and access control.


### Optional parameters

Parameter | Value
--------- | -----------
sort | The sort condition for any field. For example, `minimum ASC` if you want the results to be sorted in ascending order of the minimum value. By default the results are sorted in descending order of the `periodEnd` timestamp
page | the relative page of data to return, for ex. `2` to return 2nd page
rows | The number of records per page, for ex., `100`.
transformation | the name of the data trasformation to apply if any.
fields | an array of fields, for ex. `fields[]=temp&fields[]=pressure`. If none specified, all field values are returned
query | the filter / query in JSON format for ex, if you are only interested in weeks where the maximum co2 reported is greater than 1000 you would pass `{"maximum":{">":"1000"}}`.

### Responses

> In the example below, the data for BigAC devices are returned: <br/>
> http://www.daikinlab.com/api/data?deviceType=BigAC&apiKey=YOUR_API_KEY

```https
[
  {
    "period": 2,
    "periodEnd": "2018-08-26T07:00:00.000Z",
    "minimum": 23.31,
    "maximum": 24.73,
    "mean": 23.91604303781133,
    "median": 23.88,
    "mode": 23.34
  },
  {
    "period": 2,
    "periodEnd": "2018-08-26T00:00:00.000Z",
    "minimum": 23.41,
    "maximum": 26.15,
    "mean": 23.980652826171116,
    "median": 23.66,
    "mode": 24.09
  }
]
```

The responses are returned in JSON format. 

One of the following Error Message is returned if the operation failed:

Error | Description
--------- | -----------
NoDeviceId | Required `deviceId` missing from the request.
InvalidDeviceId | The `deviceId` value specified is invalid.
NoAttribute | Required `deviceId` missing from the request.
InvalidAttribute | The `deviceId` value specified is invalid.
NoPeriod | Required `deviceId` missing from the request.
InvalidPeriod | The `deviceId` value specified is invalid.
InvalidQuery | The `deviceId` value specified is invalid.
NoApiKey | `apiKey` missing from the request.
ExpiredApiKey | If JWT specified is expired
InvalidApiKey | If JWT specified is invalid

## Count Analytics Results

> The following returns the number of analytics computed for device DEVICE_ID where the monthly maximum temp values are greater than 30 degree celcius: 

> GET /api/analytics/count?&deviceId=DEVICE_ID&attribute=temp&period=Monthly&query={"maximum" : {">": 30}}&apiKey=YOUR_API_KEY 

<b>Count the number of rows that matches the analytics search query. </b>

You can count all or portion of the analytics computed for your devices. 
All you need to know is the device id, period, and attribute as well as any additional query filter. 

### Required parameters

Parameter | Value
--------- | -----------
deviceId | The id of the device to search analytics for
period | One of the following (case sensitive):<br/>Hourly<br/>Daily<br/>Weekly<br/>Monthly<br/>Yearly
attribute | One of the attributes with numeric values. See [Get Analytics Attributes](./#Get-Analytics-Attributes) for more info.
apiKey | Your Daikin API key for authentication and access control.

### Optional parameters

Parameter | Value
--------- | -----------
query | the filter / query in JSON format for ex, if you are only interested in weeks where the maximum co2 reported is greater than 1000 you would pass `{"maximum":{">":"1000"}}`.

### Responses

The count is returned as number if the request is successful. 

One of the following Error Message is returned if the operation failed:

Error | Description
--------- | -----------
NoDeviceTy | Required `deviceType` missing from the request.
InvalidDeviceType | The `deviceType` value specified is invalid.
NoApiKey | `apiKey` missing from the request.
ExpiredApiKey | If JWT specified is expired
InvalidApiKey | If JWT specified is invalid


# Device Management

## Device Naming 

<b>Sets a name for the device</b>
A name is a logical label by which users can reference the device instead of the more complicated and pontentially longer device ID. 
You can set device label by submitting the following API request: 
`http://www.daikinlab.com/api/device/name?deviceId=DEVICE_ID&name=DEVICE_LABEL&apiKey=YOUR_API_KEY`

### Required parameters

Parameter | Value
--------- | -----------
deviceId | Unique identifier for your device
name | Unique label to be assigned to your device
apiKey | Your Daikin API key for authentication and access control.

### Responses
The number `1` is returned if the operation is successful. 

One of the following Error Message is returned if the operation failed:

Error | Description
--------- | -----------
NoDeviceId | Required `deviceId` missing from the request.
InvalidDeviceId | No device found with the specified `deviceId`.
NoApiKey | `apiKey` missing from the request.
ExpiredApiKey | If JWT specified is expired
InvalidApiKey | If JWT specified is invalid
InvalidName | If the name specified is already used by another device.


## Device Information 

<b>Get the detailed information about a device</b> 
You can get device information as found in Daikin Cloud registry simply by submitting the following API request: 
`GET /api/device?deviceId=DEVICE_ID&apiKey=YOUR_API_KEY`
`GET /api/device?name=DEVICE_NAME&apiKey=YOUR_API_KEY`

### Required parameters

Parameter | Value
--------- | -----------
<b>One of the following:</b> |
deviceId | Unique identifier for your device
name | Unique name assigned to your device
<b>Always required:</b> |
apiKey | Your Daikin API key for authentication and access control.

### Optional parameters

Parameter | Value
--------- | -----------
fields | an array of fields, for ex. `fields[]=deviceId&fields[]=name`
includeLockedLocation | Either true or false to return the locked location or not

### Responses
The responses are returned in JSON format. 

> In the example below, all information about the device YOUR_DEVICE_ID is returned <br/>
> http://www.daikinlab.com/api/device?deviceId=YOUR_DEVICE_ID&apiKey=YOUR_API_KEYs

```https
 {
    "type": "iaqstation",
    "configuredBy": "598b7d0d2098035c42da26738148",
    "id": "5af60237d4bf1b7e12417ca2",
    "deviceId": "30000c2da690ef69d",
    "agentId": "rUpXdlh72C2Wf",
    "frequency": 600,
    "name": "AQ STATION 3.5",
    "active": true,
    "status": 1,
    "createdAt": "2018-05-11T20:51:03.334Z",
    "updatedAt": "2018-05-11T20:55:15.851Z"
}
```

> Alternatively, you can request specific device attributes by listing those fields in the request. 
> In the example below, only deviceId and name of the device is returned.<br/>
> http://www.daikinlab.com/api/device?deviceId=YOUR_DEVICE_ID&apiKey=YOUR_API_KEY&fields=deviceId&fields=name

```https
{
    "deviceId": "30000c2da690ef69d",
    "name": "AQ STATION 3.5"
}
```

> In the example below, only deviceId and name of the device is returned for the specified name. <br/>
> http://www.daikinlab.com/api/device?name=AQSTATION3.5&apiKey=YOUR_API_KEY&fields=deviceId&fields=name

```https
{
    "deviceId": "30000c2da690ef69d",
    "name": "AQ STATION 3.5"
}
```

One of the following Error Message is returned if the operation failed:

Error | Description
--------- | -----------
NoDeviceIdOrName | Either `deviceId` or name is required, both are missing from the request.
InvalidDeviceIdOrName | No device found with the specified `deviceId` or name value.
NoApiKey | `apiKey` missing from the request.
ExpiredApiKey | If JWT specified is expired
InvalidApiKey | If JWT specified is invalid

## List Device Locations 

`GET /api/device/locations?apiKey=API_KEY&deviceId=DEVICE_ID&onlyLocked=true&fields=name&fields=id`

<b>This returns the list of locations associated with the specified device.</b>

### Required parameters

Parameter | Value
--------- | -----------
deviceId | Id of an existing device.
apiKey | Your Daikin API key for authentication and access control.

### Optional parameters

Parameter | Value
--------- | -----------
fields | an array of fields to choose, for ex. `fields[]=id&fields[]=name`.
onlyLocked | Either `true` or `false`. If `true` it returns only the location where the device is currently locked to, `false` it returns all locations this device ever been to.

### Responses

> Examples: <br/>
> https://daikinlab.com/api/device/locations?apiKey=API_KEY&deviceId=DEVICE_ID <br/>
> The above returns the locations for the specified device. A sample output of a successful fetch can be as follows: 

```https
[
  {
    "name":"Tom's US Home",
    "address":"152 Anza St, Fremont, CA 94539, USA",
    "lat":37.53,
    "lon":-121.92,
    "createdAt":"2017-08-24T14:20:38.472Z",
    "updatedAt":"2017-10-31T22:50:43.115Z",
    "updatedBy":"598b79e6de473fc826456504",
    "device":"BAQ_37.53_-121.92",
    "id":"599ee0b6345aa56829b07b39"
  },
  {
    "name":"Tom's Beijing Home",
    "address":"Beijing, China",
    "lat":39.9390731,
    "lon":115.8371231,
    "createdAt":"2017-08-24T13:26:26.061Z",
    "updatedAt":"2017-12-26T10:48:03.348Z",
    "updatedBy":"598b85145d64500527a25de8",
    "device":"BAQ_39.9390731_115.8371231",
    "id":"59e6a741734d1d62dcc04b34"
  }
]
```

> https://daikinlab.com/api/customer/locations?apiKey=API_KEY&&deviceId=DEVICE_ID&fields=name&onlyLocked=true <br/>
> The above returns the name of the location where the device is currently locked to. A sample output of a successful fetch can be as follows: 

```https
[
  {
    "name":"Tom's Home"
  }
]
```

The locations are returned as an Array if there are one or more locations for this device. 
Empty array is returned if no location is associated with the specified device 

One of the following Error Message is returned if the operation failed:

Error | Description
--------- | -----------
NoDeviceId | Required `deviceId` missing from the request.
NoApiKey | `apiKey` missing from the request.
ExpiredApiKey | If JWT specified is expired
InvalidApiKey | If JWT specified is invalid


# Location Management

## Register a Location 

`POST /api/location/register?apiKey=API_KEY&name=LOCATION_NAME&lat=LAT&lon=LON`

<b>This creates or updates a Location. </b>
A Location can be the physical address or logical place to locate zero or more devices.

### Required parameters

Parameter | Value
--------- | -----------
| <b>One or more of the following:</b>
locationId | Id value of an existing location, to update its related information.
name | This could be the logical name for this location. For ex., acme headquarters. If none specified, the address will be used as the name. If `lat` and `lon` are used but no address is specified, IoT Cloud will attempt to reverse geocode `lat` and `long` to derive the address.
lat, lon | The latitude and longitude of the location.
address | Street address. Use this if you do not know the `lat` and `lon` of the location. IoT Cloud will attempt to geocode this address to derive the `lat` and `lon`.
| <b>Always required:</b>
apiKey | Your Daikin API key for authentication and access control.
type | Either `Building` or `Space`. Default is `Building`

### Optional parameters

Parameter | Value
--------- | -----------
parent | If this location is within another location. For example, a Room within a Building
customer | Customer Identifier, if the location is owned or managed by a customer.
description | Additional notes if any for this location
status | `0` if location is active, `1` if to be disabled. Default is `0`

### Responses

> Examples: <br/>
> `https://daikinlab.com/api/location/register?apiKey=API_KEY&name=Tabo Masuda's Home&lat=34.7060364&lon=135.4998511` <br/>
> The above creates a location named 'Tabo Masuda's Home' at (lat, lon) of (34.7060364, 135.4998511) 

> `https://daikinlab.com/api/location/register?apiKey=API_KEY&name=Daikin HQ&address=2 Chome-4-12 Nakazakinishi, Kita, Osaka, Osaka Prefecture 530-0015, Japan` <br/>
> The above creates a location named 'Daikin HQ' at 2 Chome-4-12 Nakazakinishi, Kita, Osaka, Osaka Prefecture 530-0015, Japan 

> `https://daikinlab.com/api/location/register?apiKey=API_KEY&locationId=mylocationId&name=Daikin HeadQuarters` <br/> 
> The above changes the name of an existing location with id mylocationId to Daikin HeadQuarters 

> `https://daikinlab.com/api/location/register?apiKey=API_KEY&locationId=mylocationId&status=1` <br/>
> The above disables the location with id mylocationId 

> `https://daikinlab.com/api/location/register?apiKey=API_KEY&name=Front Office&type=Space&parent=acmeLocationId&customer=mycustomerId` <br/>
> The above adds Front Office as a Space to an existing building location acmeLocationId within a customer mycustomerId.

The id of the location is returned if the operation is successful. 

One of the following Error Message is returned if the operation failed:

Error | Description
--------- | -----------
NoLocationInfo | if the information required to register a location is not provided.
NoApiKey | `apiKey` missing from the request.
ExpiredApiKey | If JWT specified is expired
InvalidApiKey | If JWT specified is invalid

## Get Location Details 

> Examples:<br/>
> https://daikinlab.com/api/location/info?apiKey=API_KEY&locationId=mylocationId <br/>
> The above fetches the location with id of mylocationId.

`GET /api/location/info?apiKey=API_KEY&locationId=mylocationId`

<b>This returns the Location detail. </b>
A Location can be the physical address or logical place to locate zero or more devices.

### Required parameters

Parameter | Value
--------- | -----------
| <b>One of the following:<b>
locationId | Id value of an existing location.
name | This could be the logical name for this location. For ex., acme headquarters.
lat, lon | The latitude and longitude of the location where the device is currently located.
| <b>Always required:</b>
apiKey | Your Daikin API key for authentication and access control.

### Optional parameters

Parameter | Value
--------- | -----------
customer | Customer Identifier, if looking for a customer specific location.

### Responses
The location is returned in JSON format if the operation is successful. 

> A sample output of a successful fetch can be as follows: 

```https
{
  "updatedBy":"598b79e6de473fc826456504",
  "name":"Tabo Masuda's office",
  "address":"2-30 Chayamachi, Kita-ku, Ōsaka-shi, Ōsaka-fu 530-0013, Japan",
  "lat":34.7060364,
  "lon":135.4998511,
  "createdAt":"2018-07-03T23:10:43.111Z",
  "updatedAt":"2018-07-04T13:57:51.471Z",
  "id":"5b3c02737fa3b38b517cfe68"
} 
```

One of the following Error Message is returned if the operation failed:

Error | Description
--------- | -----------
NoLocationInfo | Required information missing to find the location.
InvalidLocationInfo | if no location found matching to the information provided.
NoApiKey | `apiKey` missing from the request.
ExpiredApiKey | If JWT specified is expired
InvalidApiKey | If JWT specified is invalid

## Lock Device to a Location 

> Examples:<br/>
> https://daikinlab.com/api/location/info?apiKey=API_KEY&locationId=mylocationId&deviceId=mydeviceId <br/>
> The above locks mydeviceId to the location mylocationId.

`POST /api/location/lockdevice?apiKey=API_KEY&locationId=mylocationId&deviceId=mydeviceId`

<b>This locks the specified device to this Location. </b>
Once locked, all device reporting will be considered as coming from this location until the device is released from this location.

### Required parameters

Parameter | Value
--------- | -----------
locationId | Id value of an existing location.
deviceId | Id value of an existing device.
apiKey | Your Daikin API key for authentication and access control.

### Responses

> A sample output of a successful fetch can be as follows: 

```https
{
  "updatedBy":"598b79e6de473fc826456504",
  "name":"Tabo Masuda's office",
  "address":"2-30 Chayamachi, Kita-ku, Ōsaka-shi, Ōsaka-fu 530-0013, Japan",
  "lat":34.7060364,
  "lon":135.4998511,
  "createdAt":"2018-07-03T23:10:43.111Z",
  "updatedAt":"2018-07-04T13:57:51.471Z",
  "id":"5b3c02737fa3b38b517cfe68"
} 
```

The location is returned in JSON format if the operation is successful. 

One of the following Error Message is returned if the operation failed:

Error | Description
--------- | -----------
NoLocationId | Required `locationId` missing.
InvalidLocationInfo | if no location found matching to the information provided.
NoDeviceId | Required `deviceId` missing.
NoApiKey | `apiKey` missing from the request.
ExpiredApiKey | If JWT specified is expired
InvalidApiKey | If JWT specified is invalid


## Release the Device from a Location 

> Examples: <br/>
> https://daikinlab.com/api/location/releasedevice?apiKey=API_KEY&deviceId=mydeviceId  <br/>
> The above releases mydeviceId from the current location lock. <br/>

`POST /api/location/releasedevice?apiKey=API_KEY&deviceId=mydeviceId`

<b>This releases the specified device from the current Location. </b>
Once released, this device is available to be registered to another location.

### Required parameters

Parameter | Value
--------- | -----------
deviceId | Id value of an existing device.
apiKey | Your Daikin API key for authentication and access control.

### Responses
The number `1` is returned if the operation is successful. 

One of the following Error Message is returned if the operation failed:

Error | Description
--------- | -----------
NoDeviceId | Required `deviceId` missing.
NotLocked | If the device is not currently locked to any location.
NoApiKey | `apiKey` missing from the request.
ExpiredApiKey | If JWT specified is expired
InvalidApiKey | If JWT specified is invalid


## List Devices at a Location 

`GET /api/location/devices?apiKey=API_KEY&locationId=mylocationId`

<b>This returns the list of devices ever placed at this Location. </b>
A Location can be the physical address or logical place to locate zero or more devices. 

<aside class="notice">
Please note the same device can appear multiple times in the same location if it was assigned to the same location at different times.
</aside>

### Required parameters

Parameter | Value
--------- | -----------
locationId | Id of an existing location.
apiKey | Your Daikin API key for authentication and access control.

### Optional parameters

Parameter | Value
--------- | -----------
onlyLocked | Either `true` or `false`. If `true` it returns only the devices currently locked to this locations, `false` it returns all devices ever assigned to this location. `false` is default.
query | the filter / query in JSON format for ex, if you are only interested in devices at the location after August 10th, 2018 then you can pass `query={"createdAt":{">":"2018-07-10"}}`.

### Responses

> Examples: <br/>
> https://daikinlab.com/api/location/devices?apiKey=API_KEY&locationId=mylocationId <br/>
> The above returns the ids of the devices at the specified location. <br/>
> A sample output of a successful fetch can be as follows: <br/>

```https
["30000c2a690bdc27","20000c2a690a2b7b","30000c2a690c97ca"]
```

> https://daikinlab.com/api/location/devices?apiKey=API_KEY&locationId=mylocationId&onlyLocked=true <br/>
> The above returns the ids of the devices currently locked to the specified location. <br/>
> A sample output of a successful fetch can be as follows: <br/>

```https
["20000c2a690a2b7b"]
```

> https://daikinlab.com/api/location/devices?apiKey=API_KEY&locationId=mylocationId&&query={"createdAt":<br/>{">":"2018-07-10"}} 
> The above returns the ids of the devices assigned to the location after August 10th, 2018. <br/>
> A sample output of a successful fetch can be as follows: <br/>

```https
["30000c2a690a2b7b", "4000c2a690a2b7b"]
```

> https://daikinlab.com/api/location/devices?apiKey=API_KEY&locationId=mylocationId&&query={"updatedAt":{">=":"2018-01-10", "<=":"2018-15-10"}} <br/>
> The above returns the ids of the devices locked to or released from the location after January 10th, 2018 and before October 15th 2018. <br/>
> A sample output of a successful fetch can be as follows: <br/>

```https
["20000c2a690a2c7d", "30000c2a690a2b4b", "4000c2a690a2b7b"]
```

The ids of the devices are returned as an Array if there are one or more devices at the specified location. 
An empty Array is returned if no devices found at the specified location. 

One of the following Error Message is returned if the operation failed:

Error | Description
--------- | -----------
NoLocationId | Required `locationId` missing from the request.
NoApiKey | `apiKey` missing from the request.
ExpiredApiKey | If JWT specified is expired
InvalidApiKey | If JWT specified is invalid


## List Device Dates at a Location 

`GET /api/location/devicedates?apiKey=API_KEY&locationId=mylocationId`

<b>This returns the devices and the dates when those devices are placed at and removed from the location. </b>
This allows the caller to find out how long different devices were located at the specified location.

### Required parameters

Parameter | Value
--------- | -----------
locationId | Id of an existing location.
apiKey | Your Daikin API key for authentication and access control.

### Optional parameters

Parameter | Value
--------- | -----------
deviceType | If you are only interested in specific types device. Default is all device types.
from | If you are interested only in devices located at this location from the specified date.
to | If you are interested only in devices located at this location to the specified date.

### Responses

> Examples:<br/>
> https://daikinlab.com/api/location/devicedates?apiKey=API_KEY&locationId=mylocationId <br/>
> The above returns the ids of the devices along with when they were placed at and taken away from the specified location. <br/>
> A sample output of a successful fetch can be as follows: <br/>

```https
[
  {
    "device":"30000c2a690bdc27",
    "from":"2017-08-01T22:38:54.603Z",
    "to":"2017-08-20T22:38:55.654Z"
  },
  {
    "device":"20000c2a690a2b7b",
    "from":"2017-08-01T22:39:47.702Z",
    "to":null
  },
  {
    "device":"30000c2a690c97ca",
    "from":"2017-09-18T21:39:22.313Z",
    "to":"2017-09-26T14:32:20.393Z"
  }
]

# The null for to tells the device is still at the location.
```

> https://daikinlab.com/api/location/devicedates?apiKey=API_KEY&locationId=mylocationId&from=2018-10-01&to=2018-10-10 <br/>
> The above returns the ids of the devices at the specified location between the specified timerange. <br/>
> A sample output of a successful fetch can be as follows: <br/>

```https
[
  {
    "device":"BAQ_37.41_-121.98",
    "from":"2018-10-01T00:00:00.000Z",
    "to":"2018-10-10T00:00:00.000Z"
  },
  {
    "device":"40000c2a69151e81",
    "from":"2018-10-01T00:00:00.000Z",
    "to":"2018-10-10T00:00:00.000Z"
  },
  {
    "device":"40000c2a69151e57",
    "from":"2018-10-01T00:00:00.000Z",
    "to":"2018-10-03T22:13:39.680Z"
  }
]

# Please note the returned time ranges are cut to fit into requested time ranges.
```

> https://daikinlab.com/api/location/devicedates?apiKey=API_KEY&locationId=mylocationId&from=2018-10-01&to=2018-10-10&deviceType[]=Breezometer&deviceType[]=elichen-aqstation <br/>
> The above returns the ids of the devices of specified device types at the specified location between the specified timerange. <br/>
> A sample output of a successful fetch can be as follows: <br/>

```https
[
  {
    "device":"BAQ_37.41_-121.98",
    "from":"2018-10-01T00:00:00.000Z",
    "to":"2018-10-10T00:00:00.000Z"
  },
  {
    "device":"40000c2a69151e57",
    "from":"2018-10-01T00:00:00.000Z",
    "to":"2018-10-03T22:13:39.680Z"
  }
]

# Please note the returned time ranges are cut to fit into requested time ranges.
```

The ids of the devices along with the `from` and `to` timestamps, when those devices were at the specified location. 
If `from` and `to` are specified in the request, the time ranges for the devices are cut to fit within the requested time ranges. 

One of the following Error Message is returned if the operation failed:

Error | Description
--------- | -----------
NoLocationId | Required `locationId` missing from the request.
InvalidFrom | If `from` is specified but not a valid date.
InvalidTo | If `to` is specified but not a valid date.
FromAfterTo | If `to` specified is before `from`.
NoApiKey | `apiKey` missing from the request.
ExpiredApiKey | If JWT specified is expired
InvalidApiKey | If JWT specified is invalid


## List Child Locations 

`GET /api/location/childlocations?apiKey=API_KEY&locationId=mylocationId`

<b>This returns the list of locations that share the same parent Location.</b>
A Location can be the physical address or logical place to locate zero or more devices.

### Required parameters

Parameter | Value
--------- | -----------
locationId | Id of an existing parent location.
apiKey | Your Daikin API key for authentication and access control.

### Optional parameters

Parameter | Value
--------- | -----------
nested | Either `true` or `false`. If `true` it returns the nested child locations, `false` it returns just the immediate children.
includeDisabled | Either `true` for including the disabled child locations in the response or `false` otherwise. Default is `false`.
includeTags | Either `true` for including the group tags on the locations in the response or `false` otherwise. Default is `false`.
sort | The sort condition for any field. For example, `name ASC` if you want the results to be sorted in ascending order of the location name. By default the results are sorted in descending order of the `createdAt` timestamp.
fields | an array of fields, for ex. `fields[]=name&fields[]=id`
format | <b>It can take one of the following values: </b>
| flat: if the response to be returned as flat array
| tree: if the response to be returned as tree (default)

### Responses

> Examples: <br/>
> https://daikinlab.com/api/location/childlocations?locationId=parentLocationId&fields=name&fields=id&nested=true  <br/>
> The above returns the ids of the locations with specified parent location id.  <br/>
> A sample output of a successful fetch can be as follows:  <br/>

```https
[
  {
    "name":"Room123",
    "id":"5b96a53f065239241de1cf2d",
    "parent":"5b4f757ba9aead6b152cb55d",
    "children":[]
  },
  {
    "name":"Building1",
    "id":"5b883df8f4e18b1f7b545a40",
    "parent":"5b4f757ba9aead6b152cb55d",
    "children":[
      {
        "name":"Floor",
        "id":"5ba53ec8e7b00c804fa80b02",
        "parent":"5b883df8f4e18b1f7b545a40",
        "children":[
          {
            "name":"SpaceOnFloor2",
            "id":"5ba552c9e7b00c804fa80b19",
            "parent":"5ba53ec8e7b00c804fa80b02",
            "children":[]
          },
          {
            "name":"SpaceOnFloor1",
            "id":"5ba548bfe7b00c804fa80b15",
            "parent":"5ba53ec8e7b00c804fa80b02",
            "children":[]
          }
        ]
      }
    ]
  },
  {
    "name":"AcmeRoom2",
    "id":"5b4f7e17b28470b61503bb8a",
    "parent":"5b4f757ba9aead6b152cb55d",
    "children":[]
  }
]
```

The locations whose parent is the specified location. 
An empty Array is returned if no locations share the specified location as the parent. 

One of the following Error Message is returned if the operation failed:

Error | Description
--------- | -----------
NoLocationId | Required `locationId` missing from the request.
NoApiKey | `apiKey` missing from the request.
ExpiredApiKey | If JWT specified is expired
InvalidApiKey | If JWT specified is invalid
CyclicDependencyFound | If cyclic dependency found in the parent child location graph


## Tag Location To Group 

`POST /api/location/tag?apiKey=API_KEY&customer=CUSTOMER_ID&type=GROUP_TYPE&name=GROUP_NAME&locationId=LOCATION_ID`

<b>This adds a group tag, for the location if specified. </b>

<aside class="notice">
If no location is specified, it just adds or updates the group tag. 
Group tag are logical grouping of locations, like a Zones or Campuses or Newly Painted Area or anything else that the customer wants to group locations for analytics purposes. 
The same location can be part of 0 or more such groups.
</aside>

### Required parameters

Parameter | Value
--------- | -----------
apiKey | Your Daikin API key for authentication and access control.
customer | Id of the customer for whom the group tag is created.
type | Type of the Group. For example, `Zone`, `Campus` etc.
name | Name of the Group instance. For example `Zone-101` for a `Zone` or `Research Campus` for a `Campus`

### Optional parameters

Parameter | Value
--------- | -----------
locationId | Id of the location to add this group tag
status | `0` if group tag is active, `1` if to be disabled. Default is `0`

### Responses

> Examples:<br/>
> https://daikinlab.com/api/location/tag?apiKey=API_KEY&customer=CUSTOMER_ID&type=Zone&name=Zone56&locationId=LOCATION_ID <br/>
> The above creates a group tag of type Zone of name Zone56 for location with identifier LOCATION_ID for the customer CUSTOMER_ID <br/>

> https://daikinlab.com/api/location/tag?apiKey=API_KEY&customer=CUSTOMER_ID&type=Campus&name=Research <br/>
> The above creates if not already existing a group tag named 'Research' of type Campus. <br/>

> https://daikinlab.com/api/location/tag?apiKey=API_KEY&customer=CUSTOMER_ID&type=Campus&name=Research&status=1 <br/>
> The above updates the status value of an already existing group tag. Disabled group tag are not available for tagging or untagging locations. <br/>

The id of the group tag is returned if the operation is successful. 

One of the following Error Message is returned if the operation failed:

Error | Description
--------- | -----------
NoCustomer | if the required `customer` information is not provided.
NoType | if the required `type` information is not provided.
NoName | if the required `name` information is not provided.
InvalidLocationId | if the `locationId` provided is not valid.
InvalidStatus | if the status provided is not valid. It needs to be either `0` or `1`
ExistingTag | if the location already had the requested tag.
DisabledTag | If the specified tag is existing but is disabled
NoApiKey | `apiKey` missing from the request.
ExpiredApiKey | If JWT specified is expired
InvalidApiKey | If JWT specified is invalid


## Remove Tag From Location 

> Examples: <br/>
> https://daikinlab.com/api/location/untag?apiKey=API_KEY&tagId=TAG_ID&locationId=LOCATION_ID  <br/>
> The above removes the specified group tag from the location specified.  <br/>

`POST /api/location/untag?apiKey=API_KEY&tagId=TAG_ID&locationId=LOCATION_ID`

<b>This removes a group tag for the location specified.</b>

### Required parameters

Parameter | Value
--------- | -----------
apiKey | Your Daikin API key for authentication and access control.
tagId | Id of the group tag to be removed from the location.
locationId | Id of the location to remove the tag from.

### Responses

The number `1` is returned if the group tag is successfully removed. 

One of the following Error Message is returned if the operation failed:

Error | Description
--------- | -----------
NoTagId | if the required `tagId` is not provided.
NoLocationId | if the required `locationId` information is not provided.
InvalidTagId | if the `tagId` provided is not valid.
InvalidLocationId | if the `locationId` provided is not valid.
DisabledTag | If the specified tag is existing but is disabled
NoApiKey | `apiKey` missing from the request.
ExpiredApiKey | If JWT specified is expired
InvalidApiKey | If JWT specified is invalid


## List Group Tags 

`GET /api/location/tags?apiKey=API_KEY&customer=CUSTOMER_ID`

<b>This lists all the group tags for the specified customer.</b>

`GET /api/location/tags?apiKey=API_KEY&locationId=LOCATION_ID`

<b>This lists all the group tags for the specified location.</b>

### Required parameters

Parameter | Value
--------- | -----------
apiKey | Your Daikin API key for authentication and access control.
| <b>One of the following:</b>
customer | Id of the customer to list the group tags.
locationId | Id of the location to list the group tags.

### Optional parameters

Parameter | Value
--------- | -----------
fields | an array of fields, for ex. `fields[]=name&fields[]=type`. If none specified (not recommended), all field values are returned.

### Responses

> Examples: <br/>
> https://daikinlab.com/api/location/tags?apiKey=API_KEY&customer=CUSTOMER_ID&fields=type&fields=name  <br/>
> The above returns the list of group tags for the specified customer.  <br/>
> A sample output of a successful operation is as follows:  <br/>
 
```https
[
  {
    "type":"Zone",
    "name":"Zone56"
  },
  {
    "type":
    "Campus",
    "name":"Research"
  }
]
```

> https://daikinlab.com/api/location/tags?apiKey=API_KEY&locationId=LOCATION_ID  <br/>
> The above returns the list of group tags for the specified location.  <br/>
> A sample output of a successful operation is as follows:  <br/>

```https
[
  {
    "customer":"5b561b60832157bd06204bfa",
    "type":"Zone",
    "name":"Zone56",
    "createdBy":"598b79e6de473fc826456504",
    "status":0,
    "createdAt":"2018-10-03T13:59:02.725Z",
    "updatedAt":"2018-10-03T17:06:09.899Z",
    "updatedBy":"598b79e6de473fc826456504",
    "id":"5bb4cb260c1990db17a0990d"
  },
  {
    "customer":"5b561b60832157bd06204bfa",
    "type":"Campus",
    "name":"Rsearch",
    "createdBy":"598b79e6de473fc826456504",
    "status":0,
    "createdAt":"2018-10-03T13:59:02.832Z",
    "updatedAt":"2018-10-03T13:59:02.838Z",
    "updatedBy":"598b79e6de473fc826456504",
    "id":"5bb4cb260c1990db17a0990f"
  }
]
```

The JSON array of group tags is returned.

One of the following Error Message is returned if the operation failed:

Error | Description
--------- | -----------
NoCustomer | Both `customer` and `locationId` information is missing. One of them is required
NoApiKey | `apiKey` missing from the request.
ExpiredApiKey | If JWT specified is expired
InvalidApiKey | If JWT specified is invalid



## Fetch Latest Device Data Within Location 

`GET /api/location/fetch?apiKey=API_KEY&locationId=LOCATION_ID&nested=true&fields[]=temp&fields[]=pressure`

<b>This returns the latest device readings from specified location. </b>
The data is reported by devices currently locked to the specified location. 
This includes data from devices locked anywhere in the specified location, including in its child locations. 

### Required parameters

Parameter | Value
--------- | -----------
locationId | Id of an existing location.
apiKey | Your Daikin API key for authentication and access control.

### Optional parameters

Parameter | Value
--------- | -----------
nested | Either `true` or `false`. If `true` it returns the data for all nested child locations, `false` it returns just the data from the devices directly assigned to the location and its immediate child locations.
includeDisabled | Either `true` for including the disabled locations in the response or `false` otherwise. Default is `false`.
includeTags | Either `true` for including the group tags on the locations in the response or `false` otherwise. Default is `false`.
fields | an array of fields, for ex. `fields[]=temp&fields[]=pressure`. If none specified (not recommended), all field values are returned. Pass just none if device data is not required but just the locations and the devices locked to those locations.
transformation | the name of the data trasformation to apply if any.
sort | The sort condition for any location field. For example, `name ASC` if you want the results to be sorted in ascending order of the location name. By default the results are sorted in descending order of the `createdAt` timestamp.
skipEmpty | `true` to skip locations without any devices in the output, `false` to include the empty locations also.
format | <b>It can take one of the following values:</b>
| flat: if the response to be returned as flat array
| tree: if the response to be returned as tree (default)

### Responses

> Examples: <br/>
> https://daikinlab.com/api/location/fetch?locationId=parentLocationId&fields=co2&nested=true  <br/>
> The above returns the latest co2 readings from specified location and its child locations.  <br/>
> A sample output of a successful fetch can be as follows:  <br/>

```https
[
  {
    "id":"5b96a53f065239241de1cf2d",
    "name":"Room123",
    "description":"Great Room",
    "status":0,
    "type":"Space","
    parent":"5b4f757ba9aead6b152cb55d",
    "children":[]
  },
  {
    "id":"5b883df8f4e18b1f7b545a40",
    "name":"Building1",
    "type":"Building",
    "parent":"5b4f757ba9aead6b152cb55d",
    "children":[
      {
        "id":"5ba53ec8e7b00c804fa80b02",
        "name":"Floor",
        "status":0,
        "type":"Floor",
        "parent":"5b883df8f4e18b1f7b545a40",
        "children":[
          {
            "id":"5ba552c9e7b00c804fa80b19",
            "name":"SpaceOnFloor2",
            "status":0,
            "type":"Space",
            "parent":"5ba53ec8e7b00c804fa80b02",
            "children":[]
          },
          {
            "id":"5ba548bfe7b00c804fa80b15",
            "name":"SpaceOnFloor1",
            "status":0,
            "type":"Space",
            "parent":"5ba53ec8e7b00c804fa80b02",
            "children":[]
          }
        ]
      }
    ]
  },
  {
    "id":"5b4f7e17b28470b61503bb8a",
    "name":"AcmeRoom2",
    "type":"Room",
    "parent":"5b4f757ba9aead6b152cb55d",
    "devices":[
      {
        "deviceId":"40000c2a69151e57",
        "name":"DAH-BG8",
        "data":[
          {
            "createdAt":"2018-08-25T16:07:15.000Z",
            "temp":23.4,
            "pressure":1015.02,
            "humidity":46.8,
            "pm1":12,
            "pm25":19,
            "pm10":19,
            "co2":522,
            "o3":0,
            "tvoc":21,
            "co":0,
            "no2":26,
            "so2":0,
            "lux":5,
            "ch20":0,
            "dB_min":50,
            "dB_max":53,
            "dB_average":51,
            "id":"5b817eb4e658c9210f1d39bd"
          }
        ]
      }
    ],
    "children":[]
  }
]
```

The JSON array of locations, devices, and field values.

One of the following Error Message is returned if the operation failed:

Error | Description
--------- | -----------
NoLocationId | Required `locationId` missing from the request.
NoApiKey | `apiKey` missing from the request.
ExpiredApiKey | If JWT specified is expired
InvalidApiKey | If JWT specified is invalid
CyclicDependencyFound | If cyclic dependency found in the parent child location graph


# Customer Management

## Add a Customer 

> Examples: <br/>
> https://daikinlab.com/api/customer/add?apiKey=API_KEY&name=Acme  <br/>
> The above creates a customer named 'Acme' <br/>

`POST /api/customer/add?apiKey=API_KEY&name=CUSTOMER_NAME`

<b>This adds a new Customer.</b>
A Customer is an entity that owns zero or more Devices and Locations.

### Required parameters:

Parameter | Value
--------- | -----------
apiKey | Your Daikin API key for authentication and access control.
name | A unique name for this customer

### Optional parameters:

Parameter | Value
--------- | -----------
status | `0` if customer is active, `1` if to be disabled. Default is `0`
email | customer contact email

### Responses:

The id of the customer is returned if the operation is successful. 
Error with appropriate status value is returned if the operation failed due to any of the reasons including Invalid API Key or missing mandatory fields in the request


One of the following Error Message is returned if the operation failed:

Error | Description
--------- | -----------
NoName | Required name missing from the request.
ExistingCustomer | Customer with the specified name already exists.
NoApiKey | `apiKey` missing from the request.
ExpiredApiKey | If JWT specified is expired
InvalidApiKey | If JWT specified is invalid


## Update a Customer Info 

> Examples <br/>
> https://daikinlab.com/api/customer/update?apiKey=API_KEY&customerId=CUSTOMER_ID&status=1  <br/>
> The above disables a customer with id CUSTOMER_ID <br/>

`POST /api/customer/update?apiKey=API_KEY&customerId=CUSTOMER_ID&name=CUSTOMER_NEW_NAME&status=CUSTOMER_NEW_STATUS`

<b>This updates a Customer information. </b>
A Customer is an entity that owns zero or more `Devices` and `Locations`.

### Required parameters

Parameter | Value
--------- | -----------
apiKey | Your Daikin API key for authentication and access control.
customerId | The id of the customer to update

### Optional parameters

Parameter | Value
--------- | -----------
name | The new name for this customer
status | `0` if customer is active, `1` if to be disabled. Default is `0`

### Responses

The id of the customer is returned if the operation is successful. 
Error with appropriate status value is returned if the operation failed due to any of the reasons including Invalid API Key or missing mandatory fields in the request 

One of the following Error Message is returned if the operation failed:

Error | Description
--------- | -----------
NoCustomerId | Required `customerId` missing from the request.
NoApiKey | `apiKey` missing from the request.
ExpiredApiKey | If JWT specified is expired
InvalidApiKey | If JWT specified is invalid


## Remove a Customer 

> Examples: <br/>
> https://daikinlab.com/api/customer/delete?apiKey=API_KEY&customerId=CUSTOMER_ID  <br/>
> The above removes the previously added customer with id CUSTOMER_ID <br/>

`POST /api/customer/delete?apiKey=API_KEY&customerId=CUSTOMER_ID`

<b>This removes a Customer record previously created by the same user. </b>
A Customer is an entity that owns zero or more `Devices` and `Locations`. 
The remove operation is meant for dropping the customer record you added by mistake and it is only allowed if there are no locations or users associated with this customer.

### Required parameters:

Parameter | Value
--------- | -----------
apiKey | Your Daikin API key for authentication and access control.
customerId | The id of the customer to update

### Responses:

The number `1` is returned if the customer record was found and the removal is successful. 

One of the following Error Message is returned if the operation failed:

Error | Description
--------- | -----------
NoCustomerId | Required `customerId` missing from the request.
ExistingLocations | There are locations existing for this customer. Remove them before removing the customer
ExistingDeviceUsers | There are Device Users existing for this customer. Remove them before removing the customer
NoApiKey | `apiKey` missing from the request.
ExpiredApiKey | If JWT specified is expired
InvalidApiKey | If JWT specified is invalid



## List Customers 

`GET /api/customer/list?apiKey=API_KEY`

<b>This lists the Customer records created by the same user. </b>
A Customer is an entity that owns zero or more `Devices` and `Locations`.

### Required parameters

Parameter | Value
--------- | -----------
apiKey | Your Daikin API key for authentication and access control.

### Optional parameters

Parameter | Value
--------- | -----------
fields | an array of fields, for ex. `fields[]=name&fields[]=status`
includeDisabled | Either `true` for including the disabled customers in the response orfalse otherwise. Default is `false`

### Responses

> Examples: <br/>
> https://daikinlab.com/api/customer/list?apiKey=API_KEY  <br/>
> The above lists the previously added customers by this apiKey.  <br/>
> The expected response sample is as follows: <br/>

```https
[
  {
    "createdBy":"598b79e6de473fc826456504",
    "name":"Acme",
    "status":0,
    "createdAt":"2018-07-23T18:16:00.060Z",
    "updatedAt":"2018-08-02T13:08:54.668Z",
    "devicesStatus":0,
    "id":"5b561b60832157bd06204bfa"
  },
  {
    "createdBy":"598b79e6de473fc826456504",
    "name":"AQE",
    "status":0,
    "createdAt":"2018-07-23T18:16:15.744Z",
    "updatedAt":"2018-08-02T13:12:31.801Z",
    "devicesStatus":0,
    "id":"5b561b6f832157bd06204bfb"
  }
]
```

> https://daikinlab.com/api/customer/list?apiKey=API_KEY&fields=name&fields=devicesStatus <br/>
> The above lists the names and the aggregate status of all devices of the customers added by this apiKey. <br/>
> The expected response sample is as follows:<br/>

```https
[
  {
    "name":"Acme",
    "devicesStatus":1
  },
  {
    "name":"AQE",
    "devicesStatus":0
  }
]
```

The JSON array of customers is returned. 

One of the following Error Message is returned if the operation failed:

Error | Description
--------- | -----------
NoApiKey | `apiKey` missing from the request.
ExpiredApiKey | If JWT specified is expired
InvalidApiKey | If JWT specified is invalid


## Get Customer Details 

> https://daikinlab.com/api/customer/info?apiKey=API_KEY&customerId=mycustomerId&fields=id&fields=name <br/>
> The above fetches the id and name of the mycustomerId. <br/>
> A sample output of a successful fetch can be as follows: <br/>

```https
{
  "id":"5b4e872a47b3ad6812c3f490",
  "name":"FirstCo"
}
```

`GET /api/customer/info?apiKey=API_KEY&customerId=mycustomerId`

<b>This returns the Customer detail.</b>
A Customer is an entity that owns zero or more `Devices` and `Locations`.

### Required parameters

Parameter | Value
--------- | -----------
apiKey | Your Daikin API key for authentication and access control.
| <b>One of the following</b>
customerId | Id value of an existing customer.
name | Name of an existing customer.

### Optional parameters

Parameter | Value
--------- | -----------
fields | an array of fields, for ex. `fields[]=id&fields[]=name`

### Responses
>Examples:
> https://daikinlab.com/api/customer/info?apiKey=API_KEY&customerId=mycustomerId <br/>
> The above fetches the customer with id of mycustomerId. <br/>
> A sample output of a successful fetch can be as follows: <br/>

```https
{
  "createdBy":"598b79e6de473fc826456504",
  "name":"Acme",
  "status":0,
  "createdAt":"2018-07-23T18:16:00.060Z",
  "updatedAt":"2018-09-20T16:06:07.712Z",
  "devicesStatus":2,
  "id":"5b561b60832157bd06204bfa"
}
```

The customer is returned in JSON format if the operation is successful. 

One of the following Error Message is returned if the operation failed:

Error | Description
--------- | -----------
NoCustomerIdOrName | Required `customerId` or `name` missing from the request.
NoApiKey | `apiKey` missing from the request.
ExpiredApiKey | If JWT specified is expired
InvalidApiKey | If JWT specified is invalid



### Device status for customer
The `devicesStatus` attribute returns one of the following computed values. 

<aside class="notice">
Please note that this value is calculated based on whether customer device's are reporting data in expected intervals.
</aside>

`0`: if one or more of devices at customer's locations stopped reporting data.
`1`: if all devices at customer's locations are reporting data.
`2`: if there are no devices at any of the customer's locations.

### Status for customer
The status attribute returns one of the following values.

`0`: if customer is active / enabled.
`1`: if customer is inactive / disabled.


## Add Customer Location

> Examples: <br/>
> https://daikinlab.com/api/customer/addlocation?apiKey=API_KEY&customerId=CUSTOMER_ID&locationId=mylocationId  <br/>
> The above adds the location with id LOCATION_ID to customer with id CUSTOMER_ID. <br/>

`POST /api/customer/addlocation?apiKey=API_KEY&customerId=CUSTOMER_ID&locationId=LOCATION_ID`

<b>This add the location with id `LOCATION_ID` to customer with id `CUSTOMER_ID`. </b>
A customer can have zero or more locations. 
Only the top most level location needs to be added to the customer as child locations will be indirectly associated to the same customer.

### Required parameters

Parameter | Value
--------- | -----------
customerId | Id of an existing customer.
locationId | Id of an existing location.
apiKey | Your Daikin API key for authentication and access control.

### Responses
The number `1` is returned if the operation is successful and the location was never added before for this customer. 
The number `0` is returned if the location was already added for this customer 

One of the following Error Message is returned if the operation failed:

Error | Description
--------- | -----------
NoCustomerId | Required `customerId` missing from the request.
NoLocationId | Required `locationId` missing from the request.
InvalidCustomerId | The `customerId` specified does not exist.
InvalidLocationId | The `locationId` specified does not exist.
ChildLocationId | The `locationId` specified is a child location. Only top most locations are allowed to be added to the customer
NoApiKey | `apiKey` missing from the request.
ExpiredApiKey | If JWT specified is expired
InvalidApiKey | If JWT specified is invalid


## Remove Customer Location

> Examples:<br/>
> https://daikinlab.com/api/customer/removelocation?apiKey=API_KEY&customerId=CUSTOMER_ID&locationId=mylocationId <br/>
> The above removes the location with id LOCATION_ID from customer with id CUSTOMER_ID.<br/>

`POST /api/customer/removelocation?apiKey=API_KEY&customerId=CUSTOMER_ID&locationId=LOCATION_ID`

<b>This removes a location from the customer. </b>
A customer can have zero or more locations. 
Removing the location from the customer does not remove the location but just removes the association of the location to the customer.

### Required parameters

Parameter | Value
--------- | -----------
customerId | Id of an existing customer.
locationId | Id of an existing location.
apiKey | Your Daikin API key for authentication and access control.

### Responses

The number `1` is returned if the operation is successful and the specified location is one of the existing locatons for this customer. 
The number `0` is returned if the location was not found for this customer 

One of the following Error Message is returned if the operation failed:

Error | Description
--------- | -----------
NoCustomerId | Required `customerId` missing from the request.
NoLocationId | Required `locationId` missing from the request.
InvalidCustomerId | The `customerId` specified does not exist.
InvalidLocationId | The `locationId` specified does not exist.
NoApiKey | `apiKey` missing from the request.
ExpiredApiKey | If JWT specified is expired
InvalidApiKey | If JWT specified is invalid


## List Customer Locations 

`GET /api/customer/locations?apiKey=API_KEY&customerId=mycustomerId`

<b>This returns the list of locations associated with the specified customer.</b>

### Required parameters

Parameter | Value
--------- | -----------
customerId | Id of an existing customer.
apiKey | Your Daikin API key for authentication and access control.

### Optional parameters

Parameter | Value
--------- | -----------
nested | Either `true` or `false`. If `true` it returns the nested child locations, `false` it returns just the locations directly assigned to the customer.
includeDisabled | Either `true` for including the disabled locations in the response or `false` otherwise. Default is `false`.
includeTags | Either `true` for including the group tags on the locations in the response or `false` otherwise. Default is false.
sort | The sort condition for any location field. For example, `name ASC` if you want the results to be sorted in ascending order of the location name. By default the results are sorted in descending order of the `createdAt` timestamp.
fields | an array of fields to choose, for ex. `fields[]=id&fields[]=name`.
format | <b>It can take one of the following values</b>
| flat: if the response to be returned as flat array
| tree: if the response to be returned as tree (default)

### Responses

> Examples:<br/>
> https://daikinlab.com/api/customer/locations?apiKey=API_KEY&customerId=mycustomerId <br/>
> The above returns the locations associated within the specified customer. <br/>
> A sample output of a successful fetch can be as follows: <br/>

```https
[
  {
    "name":"Tom's US Home",
    "address":"152 Anza St, Fremont, CA 94539, USA",
    "lat":37.53,
    "lon":-121.92,
    "createdAt":"2017-08-24T14:20:38.472Z",
    "updatedAt":"2017-10-31T22:50:43.115Z",
    "updatedBy":"598b79e6de473fc826456504",
    "device":"BAQ_37.53_-121.92",
    "id":"599ee0b6345aa56829b07b39"
  },
  {
    "name":"Tom's Beijing Home",
    "address":"Beijing, China",
    "lat":39.9390731,
    "lon":115.8371231,
    "createdAt":"2017-08-24T13:26:26.061Z",
    "updatedAt":"2017-12-26T10:48:03.348Z",
    "updatedBy":"598b85145d64500527a25de8",
    "device":"BAQ_39.9390731_115.8371231",
    "id":"59e6a741734d1d62dcc04b34"
  }
]
```

> https://daikinlab.com/api/customer/locations?apiKey=API_KEY&customerId=mycustomerId&fields=name <br/>
> The above returns the name of the locations associated within the specified customer. <br/>
> A sample output of a successful fetch can be as follows: <br/>

```https
[
  {
    "name":"Tom's Home"
  },
  {
    "name":"Tom's Beijing Home"
  }
]
```

> https://daikinlab.com/api/customer/locations?apiKey=API_KEY&customerId=mycustomerId&nested=true&fields[]=name <br/>
> The above returns the locations associated within the specified customer as a tree with each parent location holding the child locations within children attribute. <br/>
> A sample output of a successful call can be as follows: <br/>

```https
[
  {
    "name":"Tabo Masuda's office",
    "id":"5b3c02737fa3b38b517cfe68",
    "children":[]
  },
  {
    "name":"AcmeHQ",
    "id":"5b4f757ba9aead6b152cb55d",
    "children":[
      {
        "name":"Room123",
        "id":"5b96a53f065239241de1cf2d",
        "parent":"5b4f757ba9aead6b152cb55d",
        "children":[]
      },
      {
        "name":"Building1",
        "id":"5b883df8f4e18b1f7b545a40",
        "parent":"5b4f757ba9aead6b152cb55d",
        "children":[
          {
            "name":"Floor",
            "id":"5ba53ec8e7b00c804fa80b02",
            "parent":"5b883df8f4e18b1f7b545a40",
            "children":[
              {
                "name":"SpaceOnFloor2",
                "id":"5ba552c9e7b00c804fa80b19",
                "parent":"5ba53ec8e7b00c804fa80b02",
                "children":[]
              },
              {
                "name":"SpaceOnFloor1",
                "id":"5ba548bfe7b00c804fa80b15",
                "parent":"5ba53ec8e7b00c804fa80b02",
                "children":[]
              }
            ]
          }
        ]
      },
      {
        "name":"AcmeRoom2",
        "id":"5b4f7e17b28470b61503bb8a",
        "parent":"5b4f757ba9aead6b152cb55d",
        "children":[]
      }
    ]
  },
  {
    "name":"L002",
    "id":"5b5e876c5207ab5445eec4d7",
    "children":[]
  },
  {
    "name":"Workflow-Test-Parent1",
    "id":"5b91aab8c8a9e994233aa458",
    "children":[
      {
        "name":"Workflow-Test-Space1-2",
        "id":"5b91af9892a7dbca231f358c",
        "parent":"5b91aab8c8a9e994233aa458",
        "children":[]
      },
      {
        "name":"Workflow-Test-Space1",
        "id":"5b91ac37c8a9e994233aa45b",
        "parent":"5b91aab8c8a9e994233aa458",
        "children":[]
      }
    ]
  },
  {
    "name":"Workflow-Test-Parent2",
    "id":"5b998f01d74be4ef44161a1a",
    "children":[
      {
        "name":"Workflow-Test-Space3",
        "id":"5b9b03694bf3242e023fb7c0",
        "parent":"5b998f01d74be4ef44161a1a",
        "children":[]
      },
      {
        "name":"Workflow-Test-Space2",
        "id":"5b998f09d74be4ef44161a1b",
        "parent":"5b998f01d74be4ef44161a1a",
        "children":[]
      }
    ]
  },
  {
    "name":"Workflow-Test-Parent3",
    "id":"5b9adf135a64b2ba4dd8038e",
    "children":[]
  }
]
```

The customer locations are returned as an Array if there are one or more locations at the specified customer. 
Empty array is returned if no location is associated with the specified customer 

One of the following Error Message is returned if the operation failed:

Error | Description
--------- | -----------
NoCustomerId | Required `customerId` missing from the request.
InvalidCustomerId | The `customerId` specified does not exist.
NoApiKey | `apiKey` missing from the request.
ExpiredApiKey | If JWT specified is expired
InvalidApiKey | If JWT specified is invalid


## List Customer Devices

`GET /api/customer/devices?apiKey=API_KEY&customerId=mycustomerId`

<b>This returns the list of all devices located at any of the customer locations.</b>

### Required parameters

Parameter | Value
--------- | -----------
customerId | Id of an existing customer.
apiKey | Your Daikin API key for authentication and access control.

### Optional parameters

Parameter | Value
--------- | -----------
fields | an array of fields to choose, for ex. `fields[]=id&fields[]=name`.
includeDisabled | Either `true` for including the devices at the disabled customer locations in the response or `false` otherwise. Default is `false`.

### Responses

> Examples:<br/>
> https://daikinlab.com/api/customer/devices?apiKey=API_KEY&customerId=mycustomerId&fields=name <br/>
> The above returns the devices associated within the specified customer. <br/>
> A sample output of a successful fetch can be as follows: <br/>

```https
[
  {
    "id":"5afb732aeb5fe14b5de17b4a",
    "name":"409F387244F4"
  },
  {
    "id":"5b5ea2145207ab5445eec50e",
    "name":"Test"
  },
  {
    "id":"5afa18de89a913d80fdbe5e8",
    "name":"testdevice5"
  }
]
```

The customer devices are returned as an Array if there are one or more devices at any of the customer locations. 
Empty array is returned if no location is associated with the specified customer. 

One of the following Error Message is returned if the operation failed:

Parameter | Value
--------- | -----------
NoCustomerId | Required `customerId` missing from the request.
InvalidCustomerId | The `customerId` specified does not exist.
NoApiKey | `apiKey` missing from the request.
ExpiredApiKey | If JWT specified is expired
InvalidApiKey | If JWT specified is invalid
CyclicDependencyFound | If cyclic dependency found in the parent child location graph


## List Device Users 

`GET /api/customer/deviceusers?apiKey=API_KEY&customerId=mycustomerId`

<b>This returns the list of Device Users associated with the specified customer.</b>

### Required parameters

Parameter | Value
--------- | -----------
customerId | Id of an existing customer.
apiKey | Your Daikin API key for authentication and access control.

### Optional parameters

Parameter | Value
--------- | -----------
fields | an array of fields to choose, for ex. `fields[]=deviceUserId&fields[]=email`

### Responses

> Examples:<br/>
> https://daikinlab.com/api/customer/deviceusers?apiKey=API_KEY&customerId=mycustomerId <br/>
> The above returns the ids of the device users associated within the specified customer. <br/>
> A sample output of a successful fetch can be as follows: <br/>

```https
[
  {
    "createdBy":"598b79e6de473fc826456504",
    "updatedBy":"598b79e6de473fc826456504",
    "customer":"5b561b60832157bd06204bfa",
    "updatedAt":"2018-07-26T00:10:37.766Z",
    "deviceUserId":"scott",
    "disabled":false,
    "rating":0,
    "createdAt":"2018-07-23T19:39:57.567Z",
    "email":"scott@play.com",
    "id":"5b562f0d0505f03d0733520e"
  },
  {
    "createdBy":"598b79e6de473fc826456504",
    "updatedBy":"598b79e6de473fc826456504",
    "customer":"5b561b60832157bd06204bfa",
    "updatedAt":"2018-07-26T00:10:54.838Z",
    "deviceUserId":"mary",
    "disabled":false,
    "rating":0,
    "createdAt":"2018-07-23T20:03:18.571Z",
    "email":"mary@lamb.com",
    "id":"5b563486aaee4ba607cc2070"
  }
]
```

> https://daikinlab.com/api/customer/deviceusers?apiKey=API_KEY&customerId=mycustomerId&fields=deviceUserId&fields[]=email <br/>
> The above returns the deviceId and email of the device users associated within the specified customer. <br/>
> A sample output of a successful fetch can be as follows: <br/>

```https
[
  {
    "deviceUserId":"scott",
    "email":"scott@play.com"
  },
  {
    "deviceUserId":"mary",
    "email":"mary@lamb.com"
  }
]
```

The ids of the Device Users are returned as an Array if there are one or more device users associated with the specified customer. 
Empty array is returned if no device user is associated with the specified customer. 

One of the following Error Message is returned if the operation failed:

Error | Description
--------- | -----------
NoCustomerId | Required `customerId` missing from the request.
InvalidCustomerId | The `customerId` specified does not exist.
NoApiKey | `apiKey` missing from the request.
ExpiredApiKey | If JWT specified is expired
InvalidApiKey | If JWT specified is invalid



# Device User Management

## Register a Device User 

> Examples:<br/>
> https://daikinlab.com/api/deviceuser/register?deviceUserId=mary&password=lamb&customer=5b561b60832157bd06204bfa <br/>
> The above registers a device user with id mary with password lamb for customer with id 5b561b60832157bd06204bfa<br/>

`POST /api/deviceuser/register?apiKey=API_KEY&deviceUserId=USER_ID&password=PASSWORD&name=Tom Smith&email=tom@maol.com`

<b>This creates or updates a Device User entry. </b>
A Device User is the user of one or more devices. 
Calling this API with an existing deviceUserId updates the record. 

### Required parameters

Parameter | Value
--------- | -----------
deviceUserId | id of this device user
email | Email id of the device user.
apiKey | Your Daikin API key for authentication and access control.

### Optional parameters

Parameter | Value
--------- | -----------
name | Full name of this device user.
password | initial password
disabled | `true` to disable this user.
customer | the id of the customer if this device user belongs to one.

### Responses

The `deviceUserId` of the registered device user is returned if the registration is successful. 

One of the following Error Message is returned if the operation failed:

Error | Description
--------- | -----------
NoDeviceUserId | Required `deviceUserId` missing from the request.
NoApiKey | `apiKey` missing from the request.
NoEmail | `email` missing from the request.
EmailUsedByAnotherDeviceUser | `email` specified is already used by another device user.
ExpiredApiKey | If JWT specified is expired
InvalidApiKey | If JWT specified is invalid


## Get Device User Details 

<b>This returns information about the specified Device User. </b>
A Device User is the user of one or more devices.

### Required parameters

Parameter | Value
--------- | -----------
| <b>One of the following:</b>
deviceUserId | id of this device user
email | Email id of the device user.
| <b>Always Required:</b>
apiKey | Your Daikin API key for authentication and access control.

### Optional parameters

Parameter | Value
--------- | -----------
fields | an array of fields, for ex. `fields[]=deviceUserId&fields[]=name`

### Responses

> Examples:<br/>
> https://daikinlab.com/api/deviceuser/info?apiKey=API_KEY&deviceUserId=mydeviceuserid <br/>
> The above fetches the device user with id of mydeviceuserid. <br/>
> A sample output of a successful fetch can be as follows: <br/>

```https
{
  "createdBy":"598b79e6de473fc826456504",
  "updatedBy":"598b79e6de473fc826456504",
  "customer":"5b561b60832157bd06204bfa",
  "updatedAt":"2018-08-29T15:55:49.777Z",
  "deviceUserId":"mary",
  "disabled":false,
  "rating":0,
  "createdAt":"2018-07-23T20:03:18.571Z",
  "email":"mary@lamb.com",
  "settings":"{\"autoVentilation\":\"0\",\"disableBetween\":\"0.00-0.00\"}",
  "name":"Mary Lamb",
  "usingTempPassword":false,
  "status":2,
  "id":"5b563486aaee4ba607cc2070"
}
```

> https://daikinlab.com/api/deviceuser/info?apiKey=API_KEY&email=mary@lamb.com&fields[]=deviceUserId&fields[]=name <br/>
> The above fetches the id and name of the device user with email mary@lamb.com. <br/>
> A sample output of a successful fetch can be as follows: <br/>

```https
{
  "name":"Mary Lamb",
  "deviceUserId":"mary"
}
```

The DeviceUser is returned in JSON format if the operation is successful. 

One of the following Error Message is returned if the operation failed:

Error | Description
--------- | -----------
NoDeviceUserIdOrEmail | Required `deviceUserId` or email missing from the request.
NoApiKey | `apiKey` missing from the request.
ExpiredApiKey | If JWT specified is expired
InvalidApiKey | If JWT specified is invalid


## Invite a Device User 

> Examples:<br/>
> https://daikinlab.com/api/deviceuser/invite?deviceUserId=mary <br/>
> The above invites a device user with id mary to use Daikin IoT services.<br/>

`POST /api/deviceuser/invite?apiKey=API_KEY&deviceUserId=USER_ID`

<b>This invites a Device User to Daikin IoT services. </b>
A Device User needs to be registered before she or he can be invited to use the services. 
Inviting a device user results in the device user receiving an email with temporary password. 


### Required parameters

Parameter | Value
--------- | -----------
deviceUserId | id of this device user to invite
apiKey | Your Daikin API key for authentication and access control.

### Responses

The number `1` is returned if the registration is successful. 

One of the following Error Message is returned if the operation failed:

Error | Description
--------- | -----------
NoDeviceUserId | Required `deviceUserId` missing from the request.
InvalidDeviceUserId | The specified `deviceUserId` is either invalid or missing.
NoApiKey | `apiKey` missing from the request.
ExpiredApiKey | If JWT specified is expired
InvalidApiKey | If JWT specified is invalid


## Assign a Device

> Examples:<br/>
> https://daikinlab.com/api/deviceuser/assign?deviceUserId=mary&deviceId=lamb&customer=5b561b60832157bd06204bfa <br/>
> The above registers a device user with id mary with password lamb for customer with id 5b561b60832157bd06204bfa<br/>

`POST /api/deviceuser/assign?apiKey=API_KEY&deviceUserId=USER_ID&deviceId=DEVICE_ID`

<b>This assigns a Device to the Device User. </b>

### Required parameters

Parameter | Value
--------- | -----------
deviceUserId | id of this device user
deviceId | id of the device to assign
apiKey | Your Daikin API key for authentication and access control.

### Responses

The number of devices that got assigned is returned if the operation is successful. 

One of the following Error Message is returned if the operation failed:

Error | Description
--------- | -----------
NoDeviceUserId | Required `deviceUserId` missing from the request.
InvalidDeviceUserId | The specified `deviceUserId` does not exist.
NoDeviceIdOrLocationId | Required `deviceId` missing from the request.
NoApiKey | `apiKey` missing from the request.
ExpiredApiKey | If JWT specified is expired
InvalidApiKey | If JWT specified is invalid


## Release a Device 

> Examples:<br/>
> https://daikinlab.com/api/deviceuser/release?deviceUserId=mary&deviceId=BGAC <br/>
> The above releases the device with id BGAC from device user mary.<br/>

`POST /api/deviceuser/release?apiKey=API_KEY&deviceUserId=USER_ID&deviceId=DEVICE_ID`

<b>This releases a Device currently assigned to the Device User. </b>

### Required parameters

Parameter | Value
--------- | -----------
deviceUserId | id of this device user
deviceId | id of the device to release
apiKey | Your Daikin API key for authentication and access control.

### Responses

The id of the released device is returned if the operation is successful. 

One of the following Error Message is returned if the operation failed:

Error | Description
--------- | -----------
NoDeviceUserId | Required `deviceUserId` missing from the request.
InvalidDeviceUserId | The specified `deviceUserId` does not exist.
NoDeviceId | Required `deviceUserId` missing from the request.
InvalidDeviceId | The specified `deviceUserId` does not exist.
DeviceNotAssigned | The specified `deviceId` is not currently assigned to the device user `deviceUserId`.
NoApiKey | `apiKey` missing from the request.
ExpiredApiKey | If JWT specified is expired
InvalidApiKey | If JWT specified is invalid


## Assign all Devices at a Location 

> Examples:<br/>
> https://daikinlab.com/api/deviceuser/assign?deviceUserId=mary&locationId=5b4f757ba9aead6b152cb55d <br/>
> The above assigns all devices at location 5b4f757ba9aead6b152cb55d to device user mary<br/>

`POST /api/deviceuser/assign?apiKey=API_KEY&deviceUserId=USER_ID&locationId=LOCATION_ID`

<b>This assigns all Devices at the specified location to the Device User. </b>


### Required parameters

Parameter | Value
--------- | -----------
deviceUserId | id of this device user
locationId | id of the location to locate the devices to assign
apiKey | Your Daikin API key for authentication and access control.

### Responses

The count of devices located and assigned. 
`0` if none found. 

<aside class="notice">
The number returned is the total devices from the location assigned its lifetime not on this call. 
This call can be repeated to assign new devices added to the location. 
It assigns one device only once to the device user. 
</aside>

One of the following Error Message is returned if the operation failed:

Parameter | Value
--------- | -----------
NoDeviceUserId | Required `deviceUserId` missing from the request.
InvalidDeviceUserId | The specified `deviceUserId` does not exist.
NoDeviceOrLocationId | Required `locationId` missing from the request.
InvalidLocationId | The specified `locationId` does not exist.
NoApiKey | `apiKey` missing from the request.
ExpiredApiKey | If JWT specified is expired
InvalidApiKey | If JWT specified is invalid


## List all Devices assigned 

`GET /api/deviceuser/devices?apiKey=API_KEY&deviceUserId=USER_ID`

<b>This lists all Devices currently assigned to the Device User. </b>

### Required parameters

Parameter | Value
--------- | -----------
deviceUserId | id of this device user
apiKey | Your Daikin API key for authentication and access control.

### Responses

> A sample output of a successful operation is as follows: 

```https
[
  "30000c2a690bdc27",
  "20000c2a690a2b7b",
  "30000c2a690c97ca"
] 
```

The ids of the devices currently assigned to the device user. 

One of the following Error Message is returned if the operation failed:

Error | Description
--------- | -----------
NoDeviceUserId | Required `deviceUserId` missing from the request.
InvalidDeviceUserId | The specified `deviceUserId` does not exist.
NoApiKey | `apiKey` missing from the request.
ExpiredApiKey | If JWT specified is expired
InvalidApiKey | If JWT specified is invalid


## Retrieve feedbacks 

`https://daikinlab.com/api/deviceuser/feedbacks?apiKey=API_KEY&deviceUserId=USER_ID`

<b>This retrieves the all the feedbacks and ratings by the Device User. </b>

### Required parameters

Parameter | Value
--------- | -----------
deviceUserId | id of the device user to retrieve the feedbacks from
apiKey | Your Daikin API key for authentication and access control.

### Optional parameters

Parameter | Value
--------- | -----------
fields | an array of fields, for ex. `fields[]=createdAt&fields[]=rating&fields[]=feedback` to return just the ratings and feedbacks. If none specified, all available field values including `deviceUserId`, `rating`, `feedback`, and `createdAt` are returned

### Responses

> A sample output of a successful operation is as follows: 

```https
[
  {
    "createdAt":"2018-08-23T16:20:35.272Z",
    "rating":5,
    "feedback":"OK"
  },
  {
    "createdAt":"2018-08-23T16:36:32.297Z",
    "rating":6,
    "feedback":"Very Good"
  }
]
```

An array of the feedbacks set by the device user in chronological order. 


## Send Push Notification 

`https://daikinlab.com/api/deviceuser/pushnotify?deviceUserId=mary&title=PUSH_TITLE&message=PUSH_MESSAGE&content=PUSH_CONTENT?apiKey=API_KEY`

<b>This sends push notifications to a device user. </b>
The push notification is sent using the Push tokens (for example, FCM) if any added by the device user. 

### Required parameters:

Parameter | Value
--------- | -----------
deviceUserId | The id of the device user to send the push notifications
title | The `title` for the push notification
message | The message text for the push notification
content | The content for the push notification
apiKey | Your Daikin API key for authentication and access control.

### Responses:

The number `1` is returned if the operation is successful. 

<aside class="notice">
This operation does not wait for the notification responses but it returns after queuing the push notifications. 
So the success does not really mean the message is delivered to the device user but just the push request was successfully submitted. 
</aside>

One of the following Error Message is returned if the operation failed:

Error | Description
--------- | -----------
NoDeviceUserId | Required `deviceUserId` missing from the request.
NoPushTokens | The specified `deviceUserId` does not have any push tokens.
NoApiKey | `apiKey` missing from the request.
ExpiredApiKey | If JWT specified is expired
InvalidApiKey | If JWT specified is invalid


# Device User Session

## Authenticate Device User 

`POST /api/deviceusersession/authenticate?deviceUserId=USER_ID&password=PASSWORD`

<b>This authenticates the Device User for further interaction with Diakin Cloud. </b>


### Required parameters

Parameter | Value
--------- | -----------
| <b>One of the following:</b>
deviceUserId | id of this device user
email | email id of this device user
| <b>Always required:</b>
password | password of this device user

### Responses

The JSON Web Token (JWT) is returned if the authentication is successful. 
The returned value is to be set in the request header of the subsequent requests as '`sessionid`' (recommended) or pass as '`sessionId`' request parameter.


One of the following Error Message is returned if the operation failed:

Error | Description
--------- | -----------
NoDeviceUserIdOrEmail | Required `deviceUserId` or email missing from the request.
NoPassword | Required `password` is missing from the request.
InvalidLogin | Either `deviceUserId` and `password` do not match.
Disabled | If either the device user or his/her company is currently disabled.


## Get Profile 

> Examples:<br/>
> https://daikinlab.com/api/api/deviceusersession/profile?&fields=customer&fields=email <br/>
> the above can produce a response like the one below:<br/>

```https
{
  "customer": "5b561b60832157bd06204bfa",
  "email": "mary@lamb.com"
}
```

`GET /api/deviceusersession/profile`

<b>This gets the profile / detail of the currently logged in device user.</b>

### Required parameters

Parameter | Value
--------- | -----------
sessionId | JWT received on authentication.

### Optional parameters

Parameter | Value
--------- | -----------
fields | an array of fields, for ex. `fields[]=name&fields[]=email` to return just the name and email of the device user. If none specified, all available field values including `deviceUserId`, `name`, `email`, and the `customer` are returned

### Responses

> A sample output of a successful operation can be as follows:

```https
{
  "deviceUserId": "mary",
  "name": "Mary Lamb",
  "email": "mary@lamb.com",
  "customer": "5b561b60832157bd06204bfa"
}
```

The user detail is returned in JSON format if the operation is successful. 

One of the following Error Message is returned if the operation failed:

Error | Description
--------- | -----------
NoSessionId | `sessionId` missing from the request.
ExpiredSessionId | If JWT specified is expired
InvalidSessionId | If JWT specified is invalid


## Change Password 

> Examples: <br/>
> https://daikinlab.com/api/deviceusersession/changepassword?sessionId=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE1MzIwNzkzOTEsImtXttWtiOiI1OThiNzllNmRlNDczZmM4MjY0NTY1MDQiLCJpYXQiOjE1MzIwMzYxOTF9.hTH8no6I3iP2QuQd89oE8ulWg9GCkCVU0uDjH4-RsMk&password=notsoawesome <br/>
> The above changes the password of the user with specified SessionId to notsoawesome.

`POST /api/deviceusersession/changepassword?sessionId=JWT&password=NEW_PASSWORD`

<b>This lets device user to change the password.</b>

### Required parameters

Parameter | Value
--------- | -----------
sessionId | JWT received on authentication.
password | The new password

### Responses

The number `1` is returned if the operation is successful. 

One of the following Error Message is returned if the operation failed:

Error | Description
--------- | -----------
NoSessionId | `sessionId` missing from the request.
NoPassword | `password` missing from the request.
ExpiredSessionId | If JWT specified is expired
InvalidSessionId | If JWT specified is invalid


## Reset Password 

> Examples:<br/>
> https://daikinlab.com/api/deviceusersession/resetpassword?deviceUserId=tom <br/>
> The above will result resetting the password of user tom to a temporary one. The temporary password will be sent to the email of the device user.

`POST /api/deviceusersession/resetpassword?deviceUserId=USER_ID`

<b>This lets the device user to request a password reset. </b>
Once the password request is received, the system shall send the user a temporary password by email. 
After logging in with the temporary password, the user needs to change the password to continue using the services.

### Required parameters

Parameter | Value
--------- | -----------
deviceUserId | User ID of the device user.

### Responses

The number `1` is returned if the operation is successful. 
One of the following Error Message is returned if the operation failed:

Error | Description
--------- | -----------
NoDeviceUserId | Required `deviceUserId` missing.
InvalidDeviceUserId | Specified `deviceUserId` is invalid or not found.


## Check Password Change Needed 

> Examples: <br/>
> https://daikinlab.com/api/deviceusersession/usingtemppassword

`GET /api/deviceusersession/usingtemppassword`

<b>This checks whether the user needs to change the password. </b>
After resetting password and authenticating with the system genered password the user needs to change password. 
This API allows the caller to check whether the user is currently authenticated with the system generated temporary password or not.

### Responses
The word `true` is returned if password change is required. The word `false` is returned otherwords. 

One of the following Error Message is returned if the operation failed:

Error | Description
--------- | -----------
NoApiKey | `apiKey` missing from the request.
ExpiredApiKey | If JWT specified is expired
InvalidApiKey | If JWT specified is invalid

## List Devices Assigned 

`GET /api/deviceusersession/devices?sessionId=SESSION_ID `
`GET /api/deviceusersession/devices?sessionId=SESSION_ID&deviceType=iaqstation `
`GET /api/deviceusersession/devices?sessionId=SESSION_ID&deviceType=iaqstation&fields[]=deviceId&fields[]=name`

<b>This lists the devices assigned to current device user. </b>

### Required parameters

Parameter | Value
--------- | -----------
sessionId | the session id as returned from successful authentication

### Optional parameters

Parameter | Value
--------- | -----------
deviceType | if you want to limit the list to specific device type.
fields | an array of fields, for ex. `fields[]=deviceId&fields[]=name` to return just the device id and name. If none specified (not recommended), all field values are returned

### Responses

> A sample output of a successful operation is as follows: 

```https
[
  {
    "deviceId": "30000c2a690bdc27",
    "name": "elichen-first-unit"
  },
  {
    "deviceId": "20000c2a690a2b7b",
    "name": "STO-Office-Air-Purifier"
  },
  {
    "deviceId": "30000c2a690c97ca",
    "name": "eLichens-ref-kit-ver2.0"
  },
  {
    "deviceId": "BAQ_37.41_-121.98",
    "name": "STO Outdoor Sensor"
  },
  {
    "deviceId": "30000c2a690ef694",
    "name": "eLichens-ref-kit-ver2.2"
  },
  {
    "deviceId": "30000c2a690cc54a",
    "name": "Elichen AQ Station 3.4"
  },
  {
    "deviceId": "30000c2a690ef696",
    "name": "Elichen AQ Station 3.2"
  },
  {
    "deviceId": "30000c2a690ef699",
    "name": "Elichen AQ Station 3.1"
  },
  {
    "deviceId": "30000c2a690c97c8",
    "name": "30000c2a690c97c8"
  },
  {
    "deviceId": "094",
    "name": "094"
  },
  {
    "deviceId": "30000c2a690ef69d",
    "name": "AQ STATION 3.5"
  },
  {
    "deviceId": "20000c2a690a3839",
    "name": "Tom's Home Air Purifier"
  },
  {
    "deviceId": "BAQ_37.53_-121.92",
    "name": "Tom's Home Outdoor Unit"
  },
  {
    "deviceId": "BAQ_39.9390731_115.8371231",
    "name": "Beijing Outdoor Sensor"
  },
  {
    "deviceId": "testdevice6",
    "name": "testdevice6-newname"
  }
]
```

The devices currently assigned to the device user. 

One of the following Error Message is returned if the operation failed:

Error | Description
--------- | -----------
NoSessionId | `sessionId` missing from the request.
ExpiredSessionId | If JWT specified is expired
InvalidSessionId | If JWT specified is invalid


## Fetch Latest Device Data 

`GET /api/deviceusersession/data?sessionId=SESSION_ID&deviceId=30000c2a690bdc27&fields[]=temp&fields[]=pressure `

`GET /api/deviceusersession/data?sessionId=SESSION_ID&fields[]=temp&fields[]=pressure`

<b>This lets the device user to request the latest data. </b>
The data is reported by devices currently assigned to the device user. 
You can request data from specific device (recommended) or from any any device where the requested properties are reported. 

### Required parameters

Parameter | Value
--------- | -----------
sessionId | the session id as returned from successful authentication

### Optional parameters

Parameter | Value
--------- | -----------
deviceId | if the values are to be fetched for a specific device
fields | an array of fields, for ex. `fields[]=temp&fields[]=pressure`. If none specified (not recommended), all field values are returned

### Responses

> A sample output of a successful operation is as follows: 

```https
{
  "temp":17.28,
  "pressure":1012.63
} 
```

The requested field values. 

One of the following Error Message is returned if the operation failed:

Error | Description
--------- | -----------
NoSessionId | `sessionId` missing from the request.
ExpiredSessionId | If JWT specified is expired
InvalidSessionId | If JWT specified is invalid
UnassignedDevice | The device specified is not assigned to the device user
NoAssignedDevices | No device assignments are found for this device user


## Query Device Data History

`GET /api/deviceusersession/query?sessionId=SESSION_ID&deviceId=30000c2a690ef699&query={"co2":{">":"1000"}} `
The above queries the data reported by specific device with id 30000c2a690ef699 

`GET /api/deviceusersession/query?sessionId=SESSION_ID&deviceType=iaqstation&query={"co2":{">":"1000"}} `
The above queries data reported across all devices of specified type iaqstation assigned to this device user.

<b>This lets the device user to query the historical data. </b>
The queries are limited to the data reported by devices currently assigned to the device user. 

### Required parameters

Parameter | Value
--------- | -----------
sessionId | the session id as returned from successful authentication
| <b>One of the following:</b>
deviceId | for data from specific device
deviceType | for data across all devices of specific type

### Optional parameters

Parameter | Value
--------- | -----------
query | The query in JSON format. for ex., `{"co2":{">":"1000"}, "temp":"25"}`
sort | The sort condition any field. For example, `temp ASC` if you want the results to be sorted in ascending order of the temp value. By default the results are sorted in descending order of the `createdAt` timestamp
page | the relative page of data to return, for ex. `2` to return 2nd page. By default, the first page is returned
rows | The number of records per page, for ex., `100`.
view | the name of the custom data view to apply if any.
transformation | the name of the data trasformation to apply if any.
fields | an array of fields, for ex. `fields[]=temp&fields[]=pressure`. If none specified (not recommended), all field values are returned

### Responses

> A sample output of a successful operation is as follows: 

```https
[
  {
    "device": "30000c2a690cc54a",
    "createdAt": "2018-05-09T06:25:15.000Z",
    "temp": 32.17,
    "pressure": 1015.3,
    "humidity": 40.9,
    "pm1": 3,
    "pm25": 5,
    "pm10": 5,
    "co2": 1327,
    "o3": 113,
    "tvoc": 950,
    "co": 0,
    "no2": 0,
    "so2": 0,
    "lux": 85,
    "ch20": 58,
    "dB_min": 29,
    "dB_max": 58,
    "dB_average": 40,
    "id": "5af2944b7662cc4d5b94124f"
  },
  {
    "device": "30000c2a690cc54a",
    "createdAt": "2018-05-09T06:15:22.000Z",
    "temp": 32.15,
    "pressure": 1015.18,
    "humidity": 43,
    "pm1": 2,
    "pm25": 4,
    "pm10": 4,
    "co2": 1328,
    "o3": 100,
    "tvoc": 997,
    "co": 0,
    "no2": 0,
    "so2": 0,
    "lux": 257,
    "ch20": 57,
    "dB_min": 30,
    "dB_max": 52,
    "dB_average": 38,
    "id": "5af291fa7662cc4d5b94124a"
  }
]
```

The requested data in JSON format. 

One of the following Error Message is returned if the operation failed:

Error | Description
--------- | -----------
NoSessionId | `sessionId` missing from the request.
ExpiredSessionId | If JWT specified is expired
InvalidSessionId | If JWT specified is invalid
InvalidQuery | The query specified is not a valid JSON
NoAssignedDevices | No device assignments are found for this device user
NoDeviceTypeOrDeviceId | Missing both `deviceType` and `deviceId`. One of them is required
UnknownDeviceType | Specified `deviceType` is invalid or not found


## Manage Preferences 

`POST /api/deviceusersession/preferences?sessionId=SESSION_ID&fanspeed=20&ventilation=often&night=dark`

The above sets three properties - `fanspeed`, `ventilation`, and `night` for the device user with values `20`, `often`, and `dark` as corresponding values. 

`POST /api/deviceusersession/preferences?sessionId=SESSION_ID&fields[]=night&fields[]=ventilation`

The above returns the requested preferences fields - `night` and `ventilation` values.

<b>This method lets the device user to either set or get the device user's preferences.</b>

<aside class="notice">
Note this is the device user level preferences and can be different from Device Settings covered below. 
These settings can be any and all preferences of the device user that a customer application would want to make use of. 
Currently there is no validation or limit on the type or number of the properties that can be saved per device user. 
We expect application developers to apply their best judgements while deciding these based on their application requirements. 
</aside>

### Required parameters

Parameter | Value
--------- | -----------
sessionId | the session id as returned from successful authentication

### Optional parameters

Parameter | Value
--------- | -----------
KEY=VALUE | to set a setting field `KEY` with `VALUE`. For ex., `ventilation=often`. Each call overwritres previously set value for the `KEY`.
fields | an array of fields, for ex. `fields[]=ventilation&fields[]=night` to query the values of specific fields. If none specified all field settings are returned as JSON

### Responses

> A sample output of a successful operation is as follows: 

```https
{
  "fanspeed":"20",
  "filter":"true"
} 
```

The requested field values. 

One of the following Error Message is returned if the operation failed:

Error | Description
--------- | -----------
NoSessionId | `sessionId` missing from the request.
ExpiredSessionId | If JWT specified is expired
InvalidSessionId | If JWT specified is invalid


## Manage Device Settings 

`POST /api/deviceusersession/devicesettings?sessionId=SESSION_ID&deviceId=MY_DEVICE&temp=24.5&power=ON `

The above sets two properties - `temp` and `power` for the device `MY_DEVICE` with values `24.5` and `ON` as corresponding values. 

`POST /api/deviceusersession/devicesettings?sessionId=SESSION_ID&deviceId=MY_DEVICE&fields[]=mode&fields[]=power `

The above returns the values for the requested settings mode and power.

<b>This method lets the device user to either set or get the settings for a specific device. </b>

<aside class="notice">
Note that the device level setting can come not just from the device user but also from other control systems. 
So by calling this method the device user could be overwriting the settings done by the control systems. 
Similary, the control systems could overwrite the settings applied by the device user also. 
</aside>

### Required parameters

Parameter | Value
--------- | -----------
sessionId | the session id as returned from successful authentication
deviceId | the id of the device to get or set the settings for

### Optional parameters

Parameter | Value
--------- | -----------
KEY=VALUE | to set a setting field `KEY` with `VALUE`. For ex., `pressure=1005`. Each call overwritres previously set value for the `KEY`.
fields | an array of fields, for ex. `fields[]=mode&fields[]=power` to query the values of specific fields. If none specified all field settings are returned as JSON

### Responses

> A sample output of a successful operation is as follows: 

```https
{
  "power":"ON",
  "temp":"24.5"
} 
```

The requested field values. 

One of the following Error Message is returned if the operation failed:

Error | Description
--------- | -----------
NoSessionId | `sessionId` missing from the request.
ExpiredSessionId | If JWT specified is expired
InvalidSessionId | If JWT specified is invalid

## Manage Feedback 

`POST /api/deviceusersession/feedback?sessionId=SESSION_ID&rating=3.2&feedback=service needs attention!`

<b>This sets and or gets the current rating and feedback. </b>

### Required parameters

Parameter | Value
--------- | -----------
sessionId | the session id as returned from successful authentication

### Optional parameters

Parameter | Value
--------- | -----------
rating | a rating value from `0.0` to `10.0`
feeback | a text feedback, less than 256 chars long

### Responses

> A sample output of a successful operation is as follows: 

```https
{
  "rating": 4.5,
  "feedback": "very good!"
} 
```

The rating and feedback currently set by the device user. 

One of the following Error Message is returned if the operation failed:

Error | Description
--------- | -----------
NoSessionId | `sessionId` missing from the request.
ExpiredSessionId | If JWT specified is expired
InvalidSessionId | If JWT specified is invalid


## Add Push Notification Token 

`POST /api/deviceusersession/addpushtoken?pushToken=MY_FCM_TOKEN`

<b>This adds a push notification token for the current device user.</b>
Push notification token allows Daikin IoT server to send push notification to this device user on his mobile application. 

### Required parameters

Parameter | Value
--------- | -----------
sessionId | the session id as returned from successful authentication
pushToken | the push token to add, for instance, the FCM token created using Google Firebase services

### Responses

The number `1` is returned if the operation is successful. 

One of the following Error Message is returned if the operation failed:

Error | Description
--------- | -----------
NoSessionId | `sessionId` missing from the request.
NoPushToken | `pushToken` missing from the request.
ExpiredSessionId | If JWT specified is expired
InvalidSessionId | If JWT specified is invalid

## Remove Push Notification Token

`POST /api/deviceusersession/removepushtoken?pushToken=MY_FCM_TOKEN`

<b>This removes a push notification token from the current device user. </b>
Push notification token allows Daikin IoT server to send push notification to this device user on his mobile application. 

### Required parameters

Parameter | Value
--------- | -----------
sessionId | the session id as returned from successful authentication
pushToken | the push token to remove, for example, the FCM token created using Google Firebase services

### Responses

The number `1` is returned if the token is removed. 

One of the following Error Message is returned if the operation failed:

Error | Description
--------- | -----------
NoSessionId | `sessionId` missing from the request.
NoPushToken | `pushToken` missing from the request.
ExpiredSessionId | If JWT specified is expired
InvalidSessionId | If JWT specified is invalid



# Workflow Execution

The Workflows are procedures spanning multiple API calls that can be executed at the server with given set of inputs without further client interactions. 
They may or may not produce any outputs. 
The advantage of Workflows is improved performance due to single server call instead of multiple API interactions from client applications.

## List Available Workflows 

`GET /api/workflow/list?apiKey=API_KEY`

<b>This lists all available workflows.</b>

### Required parameters

Parameter | Value
--------- | -----------
apiKey | JWT received on authentication.

### Responses

JSON list of all workflows 

One of the following Error Message is returned if the operation failed:

Error | Description
--------- | -----------
NoApiKey | `apiKey` missing from the request.
ExpiredApiKey | If JWT specified is expired
InvalidApiKey | If JWT specified is invalid


## Execute Workflow 

`POST /api/workflow/call?workflow=WORKFLOW_NAME&apiKey=API_KEY&params=PARAM_IN_JSON`

<b>This executes the specified workflow with given parameters.</b>

### Required parameters

Parameter | Value
--------- | -----------
apiKey | JWT received on authentication.
workflow | Workflow Name.
params | Parameter as expected by the Workflow, generally in JSON format.

### Responses

If successful, it should return the response / output from the workflow.

